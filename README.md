# Ground Station

Ground Station Software of the **Radar Recognition of Chaff Clouds** (**R2C2**) experiment for the launch campaign of the BEXUS 28/29 edition. The [REXUS/BEXUS](http://rexusbexus.net/) programme is a student programme from **DLR** and **SNSA**, the German and Swedish space agencies, in collaboration with **ESA**, which allows students from universities and higher education colleges across Europe to carry out scientific and technological experiments on research rockets and balloons.  
The experiment successfully flew in **October 2019**.

## Experiment explanation

The R2C2 (Radar Recognition of Chaff Clouds in the stratosphere) experiment concerns the **navigation of high altitude aerostatic balloons**. To navigate such balloons, it is critical to know which wind directions are available in the close vicinity of the balloon and with which intensity the wind is flowing. As of now, the only possible way to determine how the wind layers are moving around the balloon is through trial and error. This means changing the altitude of the balloon in the hopes of finding a wind current that will take it in the desired direction. The primary objective of this experiment is **to determine the speed and direction of the wind layers below the balloon** and **to analyze the gathered data in order to determine the feasibility of this method** to navigate balloons at high altitude. To do this, chaff will be used. Chaff consists of small conductive metal pieces. When released mid-air it forms a “cloud” and slowly drifts away following the air currents before eventually dispersing. This chaff cloud can be tracked with a radar and by studying the dynamics of chaff clouds, the wind direction and speed can be derived, establishing an in-time wind layer model. This experiment would be focused on atmospheric research.

## Architecture

Work in progress!!

## User Interfaces

Work in progress!!

## Coded for

This code has been designed to run in a Linux environment, specifically Ubuntu 18.04 LTS.

## Authors

This code has been designed by Guillermo Zaragoza Prous.  
This code has been implemented by Guillermo Zaragoza Prous.  
This code has been tested by the R2C2 team.  

## Contact

R2C2 Web: https://www.r2c2bexus.com/  
Guillermo Zaragoza Prous gitlab account: https://gitlab.com/mrzaragoza

