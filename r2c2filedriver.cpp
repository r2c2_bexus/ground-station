#include "r2c2filedriver.h"
#include "telemetryVariables.h"

R2C2FileDriver::R2C2FileDriver(QString fileName, QObject *parent) : QObject(parent)
{
    file.setFileName(fileName);

    isFileOpen = file.open(QIODevice::ReadWrite);
    if (isFileOpen)
    {
        QTextStream stream( &file );
        stream << "Starting to log from the Ground Station" << endl;
    }
}

void R2C2FileDriver::writeData(unsigned int typeData, QByteArray data)
{
    if(typeData == STATUS_MESSAGE)
    {
        writeStatusData(data);
    }
    else if (typeData == SENSOR_MESSAGE)
    {
        writeSensorData(data);
    }

}

void R2C2FileDriver::writeStatusData(QByteArray message)
{

    QByteArray header(message.mid(0,7));
    QByteArray data(message.mid(7,11));

    QByteArray hour_min = header.mid(0, 2);
    std::reverse(hour_min.begin(), hour_min.end());
    unsigned int hour_min_time  = hour_min.toHex().toUInt(nullptr,16);

    QByteArray sec_mil = header.mid(2, 2);
    std::reverse(sec_mil.begin(), sec_mil.end());
    unsigned int sec_mil_time  = sec_mil.toHex().toUInt(nullptr,16);

    float fTimeLastMessageReceived = hour_min_time * 100 + sec_mil_time / 100;

    uint8_t experimentStatus = ((data.at(0) & 0xC0) >> 6);

    uint8_t container1Status = ((data.at(0) & 0x30) >> 4);
    uint8_t container2Status = ((data.at(0) & 0x0C) >> 2);
    uint8_t container3Status = ((data.at(0) & 0x03));


    uint8_t temp1Status = ((data.at(1) & 0x80) >> 7);
    uint8_t temp2Status = ((data.at(1) & 0x40) >> 6);
    uint8_t temp3Status = ((data.at(1) & 0x20) >> 5);
    uint8_t temp4Status = ((data.at(1) & 0x10) >> 4);
    uint8_t temp5Status = ((data.at(1) & 0x08) >> 3);
    uint8_t temp6Status = ((data.at(1) & 0x04) >> 2);
    uint8_t humStatus = ((data.at(1) & 0x02) >> 1);
    uint8_t gpsStatus = ((data.at(1) & 0x01));

    uint8_t pressureStatus = ((data.at(2) & 0x80) >> 7);
    uint8_t motor1Status = ((data.at(2) & 0x40) >> 6);
    uint8_t motor2Status = ((data.at(2) & 0x20) >> 5);
    uint8_t servo1Status = ((data.at(2) & 0x10) >> 4);
    uint8_t servo2Status = ((data.at(2) & 0x08) >> 3);
    uint8_t servo3Status = ((data.at(2) & 0x04) >> 2);
    uint8_t cameraStatus = ((data.at(2) & 0x03));

    uint8_t stateMachineStatus = ((data.at(3) & 0xC0) >> 6);
    uint8_t lastContainerReleased = ((data.at(3) & 0x30) >> 4);
    uint8_t heater1Status = ((data.at(3) & 0x08) >> 3);
    uint8_t heater2Status = ((data.at(3) & 0x04) >> 2);
    uint8_t heater3Status = ((data.at(3) & 0x02) >> 1);
    uint8_t heater4Status = ((data.at(3) & 0x01));

    uint8_t heater5Status = ((data.at(4) & 0x80) >> 7);
    uint8_t imuStatus = ((data.at(4) & 0x40) >> 6);
    uint8_t connectionStatus = ((data.at(4) & 0x20) >> 5);

    QByteArray hour_min_last_release_bytes = data.mid(5, 2);
    std::reverse(hour_min_last_release_bytes.begin(), hour_min_last_release_bytes.end());
    unsigned int hour_min_last_release = hour_min_last_release_bytes.toHex().toUInt(nullptr,16);

    QByteArray sec_mil_last_release_bytes = data.mid(7, 2);
    std::reverse(sec_mil_last_release_bytes.begin(), sec_mil_last_release_bytes.end());
    unsigned int sec_mil_last_release  = sec_mil_last_release_bytes.toHex().toUInt(nullptr,16);

    uint32_t time_last_release = hour_min_last_release * 10000;
    time_last_release = time_last_release + sec_mil_last_release;

    QByteArray release_gap_time_bytes = data.mid(9, 2);
    std::reverse(release_gap_time_bytes.begin(), release_gap_time_bytes.end());
    unsigned int release_gap_time  = release_gap_time_bytes.toHex().toUInt(nullptr,16);

    if (isFileOpen)
    {
        QTextStream stream( &file );
        stream << "STATUS DATA || "
               << "Time: " << fTimeLastMessageReceived
               << " Experiment St: " << experimentStatus
               << " Container 1 St: " << container1Status
               << " Container 2 St: " << container2Status
               << " Container 3 St: " << container3Status
               << " Temperature 1 St: " << temp1Status
               << " Temperature 2 St: " << temp2Status
               << " Temperature 3 St: " << temp3Status
               << " Temperature 4 St: " << temp4Status
               << " Temperature 5 St: " << temp5Status
               << " Temperature 6 St: " << temp6Status
               << " Humidity St: " << humStatus
               << " GPS St: " << gpsStatus
               << " Pressure St: " << pressureStatus
               << " Motor 1 St: " << motor1Status
               << " Motor 2 St: " << motor2Status
               << " Servo 1 St: " << servo1Status
               << " Servo 2 St: " << servo2Status
               << " Servo 3 St: " << servo3Status
               << " Camera St: " << cameraStatus
               << " State Machine St: " << stateMachineStatus
               << " Last Container Released: " << lastContainerReleased
               << " Heater 1 St: " << heater1Status
               << " Heater 2 St: " << heater2Status
               << " Heater 3 St: " << heater3Status
               << " Heater 4 St: " << heater4Status
               << " Heater 5 St: " << heater5Status
               << " IMU St: " << imuStatus
               << " Connection St: " << connectionStatus
               << " Time Last Release: " << time_last_release
               << " Release Gap Time: " << release_gap_time
               << endl;
    }

}

void R2C2FileDriver::writeSensorData(QByteArray message)
{

    QByteArray header(message.mid(0,7));
    QByteArray data(message.mid(7,35));

    QByteArray hour_min = header.mid(0, 2);
    std::reverse(hour_min.begin(), hour_min.end());
    unsigned int hour_min_time  = hour_min.toHex().toUInt(nullptr,16);

    QByteArray sec_mil = header.mid(2, 2);
    std::reverse(sec_mil.begin(), sec_mil.end());
    unsigned int sec_mil_time  = sec_mil.toHex().toUInt(nullptr,16);

    float fTimeLastMessageReceived = hour_min_time * 100 + sec_mil_time / 100;

    int dataIndex = 0;

    QByteArray temp1Bytes = data.mid(dataIndex,2);
    std::reverse(temp1Bytes.begin(), temp1Bytes.end());
    float temp1 = ((float) temp1Bytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    QByteArray temp2Bytes = data.mid(dataIndex,2);
    std::reverse(temp2Bytes.begin(), temp2Bytes.end());
    float temp2 = ((float) temp2Bytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    QByteArray temp3Bytes = data.mid(dataIndex,2);
    std::reverse(temp3Bytes.begin(), temp3Bytes.end());
    float temp3 = ((float) temp3Bytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    QByteArray temp4Bytes = data.mid(dataIndex,2);
    std::reverse(temp4Bytes.begin(), temp4Bytes.end());
    float temp4 = ((float) temp4Bytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    QByteArray temp5Bytes = data.mid(dataIndex,2);
    std::reverse(temp5Bytes.begin(), temp5Bytes.end());
    float temp5 = ((float) temp5Bytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    QByteArray temp6Bytes = data.mid(dataIndex,2);
    std::reverse(temp6Bytes.begin(), temp6Bytes.end());
    float temp6 = ((float) temp6Bytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    float integersToPositive = 127.0;
    temp1 -= integersToPositive;
    temp2 -= integersToPositive;
    temp3 -= integersToPositive;
    temp4 -= integersToPositive;
    temp5 -= integersToPositive;
    temp6 -= integersToPositive;

    QByteArray humidityBytes = data.mid(dataIndex,2);
    std::reverse(humidityBytes.begin(), humidityBytes.end());
    float humidity = ((float) humidityBytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    QByteArray longitudeBytes = data.mid(dataIndex,4);
    std::reverse(longitudeBytes.begin(), longitudeBytes.end());
    unsigned int longitudeUInt = longitudeBytes.toHex().toUInt(nullptr,16);
    float longitude = (((float) longitudeUInt) / 100000) - 180.0;
    longitude = longitude / 100;
    dataIndex += 4;

    QByteArray latitudeBytes = data.mid(dataIndex,4);
    std::reverse(latitudeBytes.begin(), latitudeBytes.end());
    unsigned int latitudeUInt = latitudeBytes.toHex().toUInt(nullptr,16);
    float latitude = (((float) (latitudeUInt)) / 100000) - 180.0;
    latitude = latitude / 100;
    dataIndex += 4;

    QByteArray altitudeBytes = data.mid(dataIndex,2);
    std::reverse(altitudeBytes.begin(), altitudeBytes.end());
    unsigned int altitude = altitudeBytes.toHex().toUInt(nullptr,16);
    dataIndex += 2;

    QByteArray pressureBytes = data.mid(dataIndex,2);
    std::reverse(pressureBytes.begin(), pressureBytes.end());
    unsigned int pressure = pressureBytes.toHex().toUInt(nullptr,16);
    dataIndex += 2;

    QByteArray encoder1Bytes = data.mid(dataIndex,4);
    std::reverse(encoder1Bytes.begin(), encoder1Bytes.end());
    unsigned int encoder1 = encoder1Bytes.toHex().toUInt(nullptr,16);
    dataIndex += 4;

    QByteArray encoder2Bytes = data.mid(dataIndex,4);
    std::reverse(encoder2Bytes.begin(), encoder2Bytes.end());
    unsigned int encoder2 = encoder2Bytes.toHex().toUInt(nullptr,16);
    dataIndex += 4;

    uint8_t detectorSwitch1 = ((data.at(dataIndex) & 0x80) >> 7);
    uint8_t detectorSwitch2 = ((data.at(dataIndex) & 0x40) >> 6);
    uint8_t detectorSwitch3 = ((data.at(dataIndex) & 0x20) >> 5);

    dataIndex++;


    if (isFileOpen)
    {
        QTextStream stream( &file );
        stream.setRealNumberPrecision(8);
        stream << "SENSOR DATA || "
               << "Time: " << fTimeLastMessageReceived
               << " Temperature 1: " << temp1
               << " Temperature 2: " << temp2
               << " Temperature 3: " << temp3
               << " Temperature 4: " << temp4
               << " Temperature 5: " << temp5
               << " Temperature 6: " << temp6
               << " Humidity: " << humidity
               << " Longitude: " << longitude
               << " Latitude: " << latitude
               << " Altitude: " << altitude
               << " Pressure: " << pressure
               << " Encoder 1: " << encoder1
               << " Encoder 2: " << encoder2
               << " Detector Switch 1: " << detectorSwitch1
               << " Detector Switch 2: " << detectorSwitch2
               << " Detector Switch 3: " << detectorSwitch3
               << endl;
    }

}


R2C2FileDriver::~R2C2FileDriver()
{
    if (file.isOpen())
    {
        file.close();
    }
}
