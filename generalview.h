#ifndef GENERALVIEW_H
#define GENERALVIEW_H

#include <QWidget>
#include "commands.h"

namespace Ui {
class GeneralView;
}

class GeneralView : public QWidget
{
    Q_OBJECT

public:
    explicit GeneralView(QWidget *parent = nullptr);
    ~GeneralView();

private:
    Ui::GeneralView *ui;
};

#endif // GENERALVIEW_H
