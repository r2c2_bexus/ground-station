#ifndef GENERALTAB_H
#define GENERALTAB_H

#include "r2c2tab.h"
#include "qcustomplot.h"

namespace Ui {
class GeneralTab;
}

class GeneralTab : public R2C2Tab
{
    Q_OBJECT

public:
    explicit GeneralTab(QWidget *parent = nullptr);
    ~GeneralTab();

public slots:
    void setValue(unsigned int valueType, float newValue);

private slots:
    void on_setPassiveModeButton_clicked();

    void on_setManualModeButton_clicked();

    void on_setAutonomousModeButton_clicked();

    void on_releaseContainer1Button_clicked();

    void on_releaseContainer2Button_clicked();

    void on_releaseContainer3Button_clicked();

    void on_activateMotor1Button_clicked();

    void on_activateMotor2Button_clicked();

    void on_deactivateMotor1Button_clicked();

    void on_deactivateMotor2Button_clicked();

    void on_activateHeater1Button_clicked();

    void on_activateHeater2Button_clicked();

    void on_activateHeater3Button_clicked();

    void on_activateHeater4Button_clicked();

    void on_activateHeater5Button_clicked();

    void on_deactivateHeater1Button_clicked();

    void on_deactivateHeater2Button_clicked();

    void on_deactivateHeater3Button_clicked();

    void on_deactivateHeater4Button_clicked();

    void on_deactivateHeater5Button_clicked();

    void on_cameraSystemOnButton_clicked();

    void on_cameraSystemOffButton_clicked();

    void on_retractServo1Button_clicked();

    void on_retractServo2Button_clicked();

    void on_retractServo3Button_clicked();

    void on_cameraSystemRecordButton_clicked();

    void on_cameraSystemStopButton_clicked();

    void on_i2cBusResetButton_clicked();

    void on_changeRelGapTimeButton_clicked();

    void on_setLaunchTimeButton_clicked();

private:
    void setTemperature1(float newValue);
    void setTemperature2(float newValue);
    void setTemperature3(float newValue);
    void setTemperature4(float newValue);
    void setTemperature5(float newValue);
    void setTemperature6(float newValue);
    void setExperimentStatus(float newValue);
    void setStateMachine(float newValue);
    void setContainer1Status(float newValue);
    void setContainer2Status(float newValue);
    void setContainer3Status(float newValue);

    void setConnectionStatus(float newValue);
    void setTimeLastMessageReceived(float newValue);

    QVector<double> temp1Vector, temp2Vector, temp3Vector, temp4Vector, temp5Vector, temp6Vector;
    QTime timeLastMessageReceived;

    Ui::GeneralTab *ui;
};

#endif // GENERALTAB_H
