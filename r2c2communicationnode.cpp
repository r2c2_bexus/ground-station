#include "r2c2communicationnode.h"

#include <QDataStream>

#include "status.h"
#include "commands.h"
#include "telemetryVariables.h"

//float QByteArrayToFloat(QByteArray arr);

R2C2CommunicationNode::R2C2CommunicationNode()
{
    initSockets();
    startPing();
}

void R2C2CommunicationNode::initSockets()
{
    qDebug() << "Init socket UDP";
    udpSocket = new QUdpSocket(this);
    udpSocket->bind(7777, QUdpSocket::ShareAddress);

    qDebug() << "Init socket TCP";
    tcpSocket = new QTcpSocket(this);
    tcpSocket->connectToHost("172.16.18.131",8000);
    if ((commandingConnected = tcpSocket->waitForConnected(5000)))
        qDebug("TCP Connected!");

    connect(udpSocket, &QUdpSocket::readyRead, this, &R2C2CommunicationNode::readPendingDatagrams);

    connectionTimer = new QTimer(this);
    connect(connectionTimer, SIGNAL(timeout()), this, SLOT(checkConnection()));
    connectionTimer->start(2000);

    qDebug() << "Init socket finished";
}


void R2C2CommunicationNode::startPing()
{
    pingTimer = new QTimer(this);
    connect(pingTimer, SIGNAL(timeout()), this, SLOT(sendPing()));
    pingTimer->start(5000);
}

void R2C2CommunicationNode::checkConnection()
{
    qDebug() << "------------------ checkConnection. Commanding connected? " << commandingConnected;
    if (!commandingConnected || (tcpSocket->state() != QAbstractSocket::ConnectedState && tcpSocket->state() != QAbstractSocket::ConnectingState)){
        tcpSocket->connectToHost("172.16.18.131",8000);
        commandingConnected = tcpSocket->waitForConnected(500);
    }
    qDebug() << "*************** checkConnection. Commanding connected? " << commandingConnected;
}

void R2C2CommunicationNode::readPendingDatagrams()
{
    qDebug() << "Datagram received";
    while (udpSocket->hasPendingDatagrams()) {
        QNetworkDatagram datagram = udpSocket->receiveDatagram();
        processDatagram(datagram);
    }
}

void R2C2CommunicationNode::processDatagram(QNetworkDatagram& datagram)
{
    qDebug() << "Processing Datagram...";
    QByteArray message = datagram.data();

    QByteArray header(message.mid(0,7));
    QByteArray data(message.mid(7,19));

    char typeMessage = header.at(6) & 0x60;

    /*int size = message.size();
    for (int i = 0; i< size; i++) {
        qDebug() << "Byte " << i << ": " << QString(message.mid(i,1).toHex());
        //qDebug() << "Byte " << i << ": DEC = " << message.mid(i,1).toHex().toInt()
        //         << ", HEX = " << message.mid(i,1).toHex() << ": DEC/hex = " << message.mid(i,1).toHex().toInt(nullptr,16) << message.mid(i,1);
    }*/

    if(typeMessage == 0x00)
    {
        //qDebug() << "Sensor message";
        processSensorData(message);
    }
    else if (typeMessage == 0x20)
    {
        //qDebug() << "Status message";
        processStatusData(message);
    }
}

void R2C2CommunicationNode::processSensorData(QByteArray& message)
{
    emit dataReceived(SENSOR_MESSAGE, message);

    QByteArray header(message.mid(0,7));
    QByteArray data(message.mid(7,35));

    processHeader(header);

    int dataIndex = 0;

    QByteArray temp1Bytes = data.mid(dataIndex,2);
    std::reverse(temp1Bytes.begin(), temp1Bytes.end());
    float temp1 = ((float) temp1Bytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    QByteArray temp2Bytes = data.mid(dataIndex,2);
    std::reverse(temp2Bytes.begin(), temp2Bytes.end());
    float temp2 = ((float) temp2Bytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    QByteArray temp3Bytes = data.mid(dataIndex,2);
    std::reverse(temp3Bytes.begin(), temp3Bytes.end());
    float temp3 = ((float) temp3Bytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    QByteArray temp4Bytes = data.mid(dataIndex,2);
    std::reverse(temp4Bytes.begin(), temp4Bytes.end());
    float temp4 = ((float) temp4Bytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    QByteArray temp5Bytes = data.mid(dataIndex,2);
    std::reverse(temp5Bytes.begin(), temp5Bytes.end());
    float temp5 = ((float) temp5Bytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    QByteArray temp6Bytes = data.mid(dataIndex,2);
    std::reverse(temp6Bytes.begin(), temp6Bytes.end());
    float temp6 = ((float) temp6Bytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    /*int temp1 = data.mid(dataIndex++,1).toHex().toInt(nullptr,16);
    int temp2 = data.mid(dataIndex++,1).toHex().toInt(nullptr,16);
    int temp3 = data.mid(dataIndex++,1).toHex().toInt(nullptr,16);
    int temp4 = data.mid(dataIndex++,1).toHex().toInt(nullptr,16);
    int temp5 = data.mid(dataIndex++,1).toHex().toInt(nullptr,16);
    int temp6 = data.mid(dataIndex++,1).toHex().toInt(nullptr,16);*/

    float integersToPositive = 127.0;
    temp1 -= integersToPositive;
    temp2 -= integersToPositive;
    temp3 -= integersToPositive;
    temp4 -= integersToPositive;
    temp5 -= integersToPositive;
    temp6 -= integersToPositive;

    /*qDebug() << "Sensor Message size: " << size;
    for (int i = 0; i< size; i++) {
        qDebug() << "Byte " << i << ": DEC = " << message.mid(i,1).toHex().toInt()
                 << ", HEX = " << message.mid(i,1).toHex() << ": DEC/hex = " << message.mid(i,1).toHex().toInt(nullptr,16);
    }*/

    //unsigned int humidity = data.mid(dataIndex++,1).toHex().toUInt(nullptr,16);

    QByteArray humidityBytes = data.mid(dataIndex,2);
    std::reverse(humidityBytes.begin(), humidityBytes.end());
    float humidity = ((float) humidityBytes.toHex().toInt(nullptr,16)) / 100.0;
    dataIndex += 2;

    QByteArray longitudeBytes = data.mid(dataIndex,4);
    std::reverse(longitudeBytes.begin(), longitudeBytes.end());
    unsigned int longitudeUInt = longitudeBytes.toHex().toUInt(nullptr,16);
    float longitude = (((float) longitudeUInt) / 100000) - 180.0;
    longitude = longitude / 100;
    dataIndex += 4;

    QByteArray latitudeBytes = data.mid(dataIndex,4);
    std::reverse(latitudeBytes.begin(), latitudeBytes.end());
    unsigned int latitudeUInt = latitudeBytes.toHex().toUInt(nullptr,16);
    float latitude = (((float) (latitudeUInt)) / 100000) - 180.0;
    latitude = latitude / 100;
    dataIndex += 4;

    QByteArray altitudeBytes = data.mid(dataIndex,2);
    std::reverse(altitudeBytes.begin(), altitudeBytes.end());
    unsigned int altitude = altitudeBytes.toHex().toUInt(nullptr,16);
    dataIndex += 2;

    QByteArray pressureBytes = data.mid(dataIndex,2);
    std::reverse(pressureBytes.begin(), pressureBytes.end());
    unsigned int pressure = pressureBytes.toHex().toUInt(nullptr,16);
    dataIndex += 2;

    QByteArray encoder1Bytes = data.mid(dataIndex,4);
    std::reverse(encoder1Bytes.begin(), encoder1Bytes.end());
    unsigned int encoder1 = encoder1Bytes.toHex().toUInt(nullptr,16);
    dataIndex += 4;

    QByteArray encoder2Bytes = data.mid(dataIndex,4);
    std::reverse(encoder2Bytes.begin(), encoder2Bytes.end());
    unsigned int encoder2 = encoder2Bytes.toHex().toUInt(nullptr,16);
    dataIndex += 4;

    uint8_t detectorSwitch1 = ((data.at(dataIndex) & 0x80) >> 7);
    uint8_t detectorSwitch2 = ((data.at(dataIndex) & 0x40) >> 6);
    uint8_t detectorSwitch3 = ((data.at(dataIndex) & 0x20) >> 5);

    //qDebug() << "---------------------------------------------------------------------Byte " << data.mid(dataIndex,1).toHex().toUInt(nullptr, 16);
    dataIndex++;

    /*qDebug() << "Temperature 1: " << temp1;
    qDebug() << "Temperature 2: " << temp2;
    qDebug() << "Temperature 3: " << temp3;
    qDebug() << "Temperature 4: " << temp4;
    qDebug() << "Temperature 5: " << temp5;
    qDebug() << "Temperature 6: " << temp6;

    qDebug() << "Humidity: " << humidity;
    qDebug() << "Longitude: " << longitude;
    qDebug() << "Latitude: " << latitude;
    qDebug() << "Altitude: " << altitude;
    qDebug() << "Pressure: " << pressure;*/


    /*qDebug() << "Encoder 1: " << encoder1;
    qDebug() << "Encoder 2: " << encoder2;*/


    /*qDebug() << "Detector Switch 1: " << detectorSwitch1;
    qDebug() << "Detector Switch 2: " << detectorSwitch2;
    qDebug() << "Detector Switch 3: " << detectorSwitch3;*/

    emit valueChanged(TEMPERATURE_SENS_1_ID, temp1);
    emit valueChanged(TEMPERATURE_SENS_2_ID, temp2);
    emit valueChanged(TEMPERATURE_SENS_3_ID, temp3);
    emit valueChanged(TEMPERATURE_SENS_4_ID, temp4);
    emit valueChanged(TEMPERATURE_SENS_5_ID, temp5);
    emit valueChanged(TEMPERATURE_SENS_6_ID, temp6);

    emit valueChanged(HUMIDITY_SENS_ID, humidity);
    emit valueChanged(GPS_LONGITUDE_ID, longitude);
    emit valueChanged(GPS_LATITUDE_ID, latitude);
    emit valueChanged(GPS_ALTITUDE_ID, altitude);
    emit valueChanged(PRESSURE_SENS_ID, pressure);

    emit valueChanged(ENCODER_1_PULSES_ID, encoder1);
    emit valueChanged(ENCODER_2_PULSES_ID, encoder2);

    emit valueChanged(DETECTOR_SWITCH_SENS_1_ID, detectorSwitch1);
    emit valueChanged(DETECTOR_SWITCH_SENS_2_ID, detectorSwitch2);
    emit valueChanged(DETECTOR_SWITCH_SENS_3_ID, detectorSwitch3);
}

void R2C2CommunicationNode::processStatusData(QByteArray& message)
{
    emit dataReceived(STATUS_MESSAGE, message);

    QByteArray header(message.mid(0,7));
    QByteArray data(message.mid(7,11));

    processHeader(header);

    uint8_t experimentStatus = ((data.at(0) & 0xC0) >> 6);

    uint8_t container1Status = ((data.at(0) & 0x30) >> 4);
    uint8_t container2Status = ((data.at(0) & 0x0C) >> 2);
    uint8_t container3Status = ((data.at(0) & 0x03));

    emit valueChanged(EXPERIMENT_STATUS_ID, experimentStatus);

    emit valueChanged(CONTAINER_1_STATUS_ID, container1Status);
    emit valueChanged(CONTAINER_2_STATUS_ID, container2Status);
    emit valueChanged(CONTAINER_3_STATUS_ID, container3Status);

    /*qDebug() << "EXPERIMENT STATUS = " << experimentStatus;
    qDebug() << "CONTAINER 1 STATUS = " << container1Status;
    qDebug() << "CONTAINER 2 STATUS = " << container2Status;
    qDebug() << "CONTAINER 3 STATUS = " << container3Status;*/

    uint8_t temp1Status = ((data.at(1) & 0x80) >> 7);
    uint8_t temp2Status = ((data.at(1) & 0x40) >> 6);
    uint8_t temp3Status = ((data.at(1) & 0x20) >> 5);
    uint8_t temp4Status = ((data.at(1) & 0x10) >> 4);
    uint8_t temp5Status = ((data.at(1) & 0x08) >> 3);
    uint8_t temp6Status = ((data.at(1) & 0x04) >> 2);
    uint8_t humStatus = ((data.at(1) & 0x02) >> 1);
    uint8_t gpsStatus = ((data.at(1) & 0x01));

    /*qDebug() << "TEMP 1 STATUS = " << temp1Status;
    qDebug() << "TEMP 2 STATUS = " << temp2Status;
    qDebug() << "TEMP 3 STATUS = " << temp3Status;
    qDebug() << "TEMP 4 STATUS = " << temp4Status;
    qDebug() << "TEMP 5 STATUS = " << temp5Status;
    qDebug() << "TEMP 6 STATUS = " << temp6Status;*/

    emit valueChanged(TEMPERATURE_1_STATUS_ID, temp1Status);
    emit valueChanged(TEMPERATURE_2_STATUS_ID, temp2Status);
    emit valueChanged(TEMPERATURE_3_STATUS_ID, temp3Status);
    emit valueChanged(TEMPERATURE_4_STATUS_ID, temp4Status);
    emit valueChanged(TEMPERATURE_5_STATUS_ID, temp5Status);
    emit valueChanged(TEMPERATURE_6_STATUS_ID, temp6Status);

    /*qDebug() << "HUM STATUS = " << humStatus;
    qDebug() << "GPS STATUS = " << gpsStatus;*/

    emit valueChanged(HUMIDITY_STATUS_ID, humStatus);
    emit valueChanged(GPS_STATUS_ID, gpsStatus);

    uint8_t pressureStatus = ((data.at(2) & 0x80) >> 7);
    uint8_t motor1Status = ((data.at(2) & 0x40) >> 6);
    uint8_t motor2Status = ((data.at(2) & 0x20) >> 5);
    uint8_t servo1Status = ((data.at(2) & 0x10) >> 4);
    uint8_t servo2Status = ((data.at(2) & 0x08) >> 3);
    uint8_t servo3Status = ((data.at(2) & 0x04) >> 2);
    uint8_t cameraStatus = ((data.at(2) & 0x03));

    /*qDebug() << "PRESSURE STATUS = " << pressureStatus;
    qDebug() << "MOTOR 1 STATUS = " << motor1Status;
    qDebug() << "MOTOR 2 STATUS = " << motor2Status;
    qDebug() << "SERVO 1 STATUS = " << servo1Status;
    qDebug() << "SERVO 2 STATUS = " << servo2Status;
    qDebug() << "SERVO 3 STATUS = " << servo3Status;
    qDebug() << "CAMERA STATUS = " << cameraStatus;*/

    emit valueChanged(PRESSURE_STATUS_ID, pressureStatus);
    emit valueChanged(MOTOR_1_STATUS_ID, motor1Status);

    emit valueChanged(MOTOR_2_STATUS_ID, motor2Status);
    emit valueChanged(SERVO_1_STATUS_ID, servo1Status);
    emit valueChanged(SERVO_2_STATUS_ID, servo2Status);
    emit valueChanged(SERVO_3_STATUS_ID, servo3Status);

    emit valueChanged(CAMERA_STATUS_ID, cameraStatus);

    uint8_t stateMachineStatus = ((data.at(3) & 0xC0) >> 6);
    uint8_t lastContainerReleased = ((data.at(3) & 0x30) >> 4);
    uint8_t heater1Status = ((data.at(3) & 0x08) >> 3);
    uint8_t heater2Status = ((data.at(3) & 0x04) >> 2);
    uint8_t heater3Status = ((data.at(3) & 0x02) >> 1);
    uint8_t heater4Status = ((data.at(3) & 0x01));

    /*qDebug() << "STATE MACHINE STATUS = " << stateMachineStatus;
    qDebug() << "LAST CONTAINER RELEASED = " << lastContainerReleased;
    qDebug() << "HEATER 1 STATUS = " << heater1Status;
    qDebug() << "HEATER 2 STATUS = " << heater2Status;
    qDebug() << "HEATER 3 STATUS = " << heater3Status;
    qDebug() << "HEATER 4 STATUS = " << heater4Status;*/

    emit valueChanged(STATE_MACHINE_ID, stateMachineStatus);
    emit valueChanged(LAST_CONTAINER_RELEASED_ID, lastContainerReleased);

    emit valueChanged(HEATER_1_STATUS_ID, heater1Status);
    emit valueChanged(HEATER_2_STATUS_ID, heater2Status);
    emit valueChanged(HEATER_3_STATUS_ID, heater3Status);
    emit valueChanged(HEATER_4_STATUS_ID, heater4Status);

    uint8_t heater5Status = ((data.at(4) & 0x80) >> 7);
    uint8_t imuStatus = ((data.at(4) & 0x40) >> 6);
    uint8_t connectionStatus = ((data.at(4) & 0x20) >> 5);

    /*qDebug() << "HEATER 5 STATUS = " << heater5Status;
    qDebug() << "IMU STATUS = " << imuStatus;
    qDebug() << "CONNECTION STATUS = " << connectionStatus;*/
    emit valueChanged(HEATER_5_STATUS_ID, heater5Status);
    emit valueChanged(IMU_STATUS_ID, imuStatus);
    emit valueChanged(COMM_CONNECTION_ID, connectionStatus);
    //qDebug() << "--------------Byte " << data.mid(4,1).toHex().toUInt(nullptr, 16);

    QByteArray hour_min_last_release_bytes = data.mid(5, 2);
    std::reverse(hour_min_last_release_bytes.begin(), hour_min_last_release_bytes.end());
    unsigned int hour_min_last_release = hour_min_last_release_bytes.toHex().toUInt(nullptr,16);

    QByteArray sec_mil_last_release_bytes = data.mid(7, 2);
    std::reverse(sec_mil_last_release_bytes.begin(), sec_mil_last_release_bytes.end());
    unsigned int sec_mil_last_release  = sec_mil_last_release_bytes.toHex().toUInt(nullptr,16);

    uint32_t time_last_release = hour_min_last_release * 10000;
    time_last_release = time_last_release + sec_mil_last_release;
    //qDebug() << "TIME LAST RELEASE = " << time_last_release;

    emit valueChanged(TIME_LAST_CONTAINER_RELEASED_ID, time_last_release);

    QByteArray release_gap_time_bytes = data.mid(9, 2);
    std::reverse(release_gap_time_bytes.begin(), release_gap_time_bytes.end());
    unsigned int release_gap_time  = release_gap_time_bytes.toHex().toUInt(nullptr,16);
    //qDebug() << "RELEASE GAP TIME = " << release_gap_time;

    emit valueChanged(RELEASE_GAP_TIME_ID, release_gap_time);

    /*qDebug() << "Status Message size: " << size;
    for (int i = 0; i< size; i++) {
        qDebug() << "Byte " << i << ": DEC = " << message.mid(i,1).toHex().toInt()
                 << ", HEX = " << message.mid(i,1).toHex() << ": DEC/hex = " << message.mid(i,1).toHex().toInt(nullptr,16);
    }*/

}

void R2C2CommunicationNode::processHeader(QByteArray &header)
{
    QByteArray hour_min = header.mid(0, 2);
    std::reverse(hour_min.begin(), hour_min.end());
    unsigned int hour_min_time  = hour_min.toHex().toUInt(nullptr,16);

    QByteArray sec_mil = header.mid(2, 2);
    std::reverse(sec_mil.begin(), sec_mil.end());
    unsigned int sec_mil_time  = sec_mil.toHex().toUInt(nullptr,16);

    float fTimeLastMessageReceived = hour_min_time * 100 + sec_mil_time / 100;
    emit valueChanged(COMM_LASTMESSAGERECEIVED, fTimeLastMessageReceived);
}

void R2C2CommunicationNode::sendCommand(char command)
{
    qDebug() << "Command to send: " << command;
    if(!commandingConnected)
    {
        tcpSocket->connectToHost("172.16.18.131",8000);
        commandingConnected = tcpSocket->waitForConnected(500);
    }

    if(commandingConnected)
    {
        int commandSize = 10; //TODO: Change to a proper place. Command size should not be defined here.
        QByteArray message;
        message.resize(commandSize);
        for (int i = 0;i < commandSize; i++) {
            message[i] = 0x00;
        }

        QTime currentTime = QTime::currentTime();
        qDebug() << "Current Hour: " << currentTime.hour();
        qDebug() << "Current Minute: " << currentTime.minute();
        qDebug() << "Current Second: " << currentTime.second();

        int hour_min = currentTime.hour() * 100 + currentTime.minute();
        message[0] = ((hour_min >> 8) & 0xFF);
        message[1] = (hour_min & 0xFF);

        int sec_mil = currentTime.second() * 100;
        message[2] = ((sec_mil >> 8) & 0xFF);
        message[3] = (sec_mil & 0xFF);

        message[6] = 0x40; // COMMAND in type_message (SED document).
        message[7] = command;

        qint64 numberBytesWritten = tcpSocket->write(message);

        if (numberBytesWritten <= 0)
        {
            qDebug() << "Command not sent";
            commandingConnected = false;
        }
        else if (numberBytesWritten == commandSize)
        {
            qDebug() << "Command sent. Num bytes sent: " << numberBytesWritten;
        }
    }
}

void R2C2CommunicationNode::sendCommandWithVariable(char command, uint16_t value)
{
    qDebug() << "Command to send: " << command<< " with value " << value;
    if(!commandingConnected)
    {
        tcpSocket->connectToHost("172.16.18.131",8000);
        commandingConnected = tcpSocket->waitForConnected(500);
    }

    if(commandingConnected)
    {
        int commandSize = 10; //TODO: Change to a proper place. Command size should not be defined here.
        QByteArray message;
        message.resize(commandSize);
        for (int i = 0;i < commandSize; i++) {
            message[i] = 0x00;
        }

        QTime currentTime = QTime::currentTime();
        qDebug() << "Current Hour: " << currentTime.hour();
        qDebug() << "Current Minute: " << currentTime.minute();
        qDebug() << "Current Second: " << currentTime.second();

        int hour_min = currentTime.hour() * 100 + currentTime.minute();
        message[0] = ((hour_min >> 8) & 0xFF);
        message[1] = (hour_min & 0xFF);

        int sec_mil = currentTime.second() * 100;
        message[2] = ((sec_mil >> 8) & 0xFF);
        message[3] = (sec_mil & 0xFF);

        message[6] = 0x40; // COMMAND in type_message (SED document).
        message[7] = command;

        message[8] = ((value >> 8) & 0xFF);
        message[9] = (value & 0xFF);

        qint64 numberBytesWritten = tcpSocket->write(message);

        if (numberBytesWritten <= 0)
        {
            qDebug() << "Command not sent";
            commandingConnected = false;
        }
        else if (numberBytesWritten == commandSize)
        {
            qDebug() << "Command sent. Num bytes sent: " << numberBytesWritten;
        }
    }
}

void R2C2CommunicationNode::sendPing()
{
    qDebug() << "+++++++++++++++++++++++++++++++++++++++ Sending ping. Commanding connected? " << commandingConnected;
    if(!commandingConnected)
    {
        tcpSocket->connectToHost("172.16.18.131",8000);
        commandingConnected = tcpSocket->waitForConnected(500);
    }

    if(commandingConnected)
    {
        int commandSize = 10; //TODO: Change to a proper place. Command size should not be defined here.
        QByteArray message;
        message.resize(commandSize);
        for (int i = 0;i < commandSize; i++) {
            message[i] = 0x00;
        }

        QTime currentTime = QTime::currentTime();
        qDebug() << "Current Hour: " << currentTime.hour();
        qDebug() << "Current Minute: " << currentTime.minute();
        qDebug() << "Current Second: " << currentTime.second();

        int hour_min = currentTime.hour() * 100 + currentTime.minute();
        message[0] = ((hour_min >> 8) & 0xFF);
        message[1] = (hour_min & 0xFF);

        int sec_mil = currentTime.second() * 100;
        message[2] = ((sec_mil >> 8) & 0xFF);
        message[3] = (sec_mil & 0xFF);

        message[6] = 0x60; // PING in type_message (SED document).
        message[7] = 0x00;

        qint64 numberBytesWritten = tcpSocket->write(message);

        if (numberBytesWritten <= 0)
        {
            qDebug() << "Ping not sent";
            commandingConnected = false;
        }
        else if (numberBytesWritten == commandSize)
        {
            qDebug() << "Ping sent. Num bytes sent: " << numberBytesWritten;
        }
    }
}
