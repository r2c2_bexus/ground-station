#include "telemetryTab.h"
#include "ui_telemetryTab.h"

#include <QDebug>
#include "commands.h"
#include "telemetryVariables.h"
#include "status.h"

TelemetryTab::TelemetryTab(QWidget *parent) :
    R2C2Tab(parent),
    ui(new Ui::TelemetryTab)
{
    ui->setupUi(this);
    this->setWindowTitle("R2C2 Ground Station");
}

TelemetryTab::~TelemetryTab()
{
    delete ui;
}

void TelemetryTab::setValue(unsigned int valueType, float newValue)
{
    QString value;
    value.setNum(newValue);

    switch (valueType) {
    // SENSOR VALUES
    case TEMPERATURE_SENS_1_ID:
        setTemperature1(value);
        break;
    case TEMPERATURE_SENS_2_ID:
        setTemperature2(value);
        break;
    case TEMPERATURE_SENS_3_ID:
        setTemperature3(value);
        break;
    case TEMPERATURE_SENS_4_ID:
        setTemperature4(value);
        break;
    case TEMPERATURE_SENS_5_ID:
        setTemperature5(value);
        break;
    case TEMPERATURE_SENS_6_ID:
        setTemperature6(value);
        break;
    case HUMIDITY_SENS_ID:
        setHumidity(value);
        break;
    case GPS_LONGITUDE_ID:
        value.setNum(newValue, 'f', 10);
        setLongitude(value);
        break;
    case GPS_LATITUDE_ID:
        value.setNum(newValue, 'f', 10);
        setLatitude(value);
        break;
    case GPS_ALTITUDE_ID:
        setAltitude(value);
        break;
    case PRESSURE_SENS_ID:
        setPressure(value);
        break;
    case ENCODER_1_PULSES_ID:
        setEncoder1(value);
        break;
    case ENCODER_2_PULSES_ID:
        setEncoder2(value);
        break;
    case DETECTOR_SWITCH_SENS_1_ID:
        setDetectorSwitch1(newValue);
        break;
    case DETECTOR_SWITCH_SENS_2_ID:
        setDetectorSwitch2(newValue);
        break;
    case DETECTOR_SWITCH_SENS_3_ID:
        setDetectorSwitch3(newValue);
        break;
    // STATUS VALUES
    case EXPERIMENT_STATUS_ID:
        setExperimentStatus(newValue);
        break;
    case STATE_MACHINE_ID:
        setStateMachine(newValue);
        break;
    case CONTAINER_1_STATUS_ID:
        setContainer1Status(newValue);
        break;
    case CONTAINER_2_STATUS_ID:
        setContainer2Status(newValue);
        break;
    case CONTAINER_3_STATUS_ID:
        setContainer3Status(newValue);
        break;
    case LAST_CONTAINER_RELEASED_ID:
        setLastContainerReleased(newValue);
        break;
    case TEMPERATURE_1_STATUS_ID:
        setTemperature1Status(newValue);
        break;
    case TEMPERATURE_2_STATUS_ID:
        setTemperature2Status(newValue);
        break;
    case TEMPERATURE_3_STATUS_ID:
        setTemperature3Status(newValue);
        break;
    case TEMPERATURE_4_STATUS_ID:
        setTemperature4Status(newValue);
        break;
    case TEMPERATURE_5_STATUS_ID:
        setTemperature5Status(newValue);
        break;
    case TEMPERATURE_6_STATUS_ID:
        setTemperature6Status(newValue);
        break;
    case HUMIDITY_STATUS_ID:
        setHumidityStatus(newValue);
        break;
    case GPS_STATUS_ID:
        setGPSStatus(newValue);
        break;
    case IMU_STATUS_ID:
        setIMUStatus(newValue);
        break;
    case PRESSURE_STATUS_ID:
        setPressureStatus(newValue);
        break;
    case MOTOR_1_STATUS_ID:
        setMotor1Status(newValue);
        break;
    case MOTOR_2_STATUS_ID:
        setMotor2Status(newValue);
        break;
    case SERVO_1_STATUS_ID:
        setServo1Status(newValue);
        break;
    case SERVO_2_STATUS_ID:
        setServo2Status(newValue);
        break;
    case SERVO_3_STATUS_ID:
        setServo3Status(newValue);
        break;
    case CAMERA_STATUS_ID:
        setCameraSystemStatus(newValue);
        break;
    case HEATER_1_STATUS_ID:
        setHeater1Status(newValue);
        break;
    case HEATER_2_STATUS_ID:
        setHeater2Status(newValue);
        break;
    case HEATER_3_STATUS_ID:
        setHeater3Status(newValue);
        break;
    case HEATER_4_STATUS_ID:
        setHeater4Status(newValue);
        break;
    case HEATER_5_STATUS_ID:
        setHeater5Status(newValue);
        break;
    case TIME_LAST_CONTAINER_RELEASED_ID:
        setTimeLastContainerReleased(newValue);
        break;
    case RELEASE_GAP_TIME_ID:
        setReleaseGapTime(newValue);
        break;
    }
}

void TelemetryTab::setTemperature1(QString newValue)
{
    qDebug() << "setTemperature1: " << newValue;
    ui->temp1LabelValue->setText(newValue);
}

void TelemetryTab::setTemperature2(QString newValue)
{
    qDebug() << "setTemperature2: " << newValue;
    ui->temp2LabelValue->setText(newValue);
}

void TelemetryTab::setTemperature3(QString newValue)
{
    qDebug() << "setTemperature3: " << newValue;
    ui->temp3LabelValue->setText(newValue);
}

void TelemetryTab::setTemperature4(QString newValue)
{
    qDebug() << "setTemperature4: " << newValue;
    ui->temp4LabelValue->setText(newValue);
}

void TelemetryTab::setTemperature5(QString newValue)
{
    qDebug() << "setTemperature5: " << newValue;
    ui->temp5LabelValue->setText(newValue);
}

void TelemetryTab::setTemperature6(QString newValue)
{
    qDebug() << "setTemperature6: " << newValue;
    ui->temp6LabelValue->setText(newValue);
}

void TelemetryTab::setHumidity(QString newValue)
{
    qDebug() << "setHumidity: " << newValue;
    ui->humidityLabelValue->setText(newValue);
}

void TelemetryTab::setLongitude(QString newValue)
{
    qDebug() << "setLongitude: " << newValue;
    ui->longitudeLabelValue->setText(newValue);
}

void TelemetryTab::setLatitude(QString newValue)
{
    qDebug() << "setLatitude: " << newValue;
    ui->latitudeLabelValue->setText(newValue);
}

void TelemetryTab::setAltitude(QString newValue)
{
    qDebug() << "setAltitude: " << newValue;
    ui->altitudeLabelValue->setText(newValue);
}

void TelemetryTab::setPressure(QString newValue)
{
    qDebug() << "setPressure: " << newValue;
    ui->pressureLabelValue->setText(newValue);
}

void TelemetryTab::setEncoder1(QString newValue)
{
    qDebug() << "setEncoder1: " << newValue;
    ui->encoder1LabelValue->setText(newValue);
}

void TelemetryTab::setEncoder2(QString newValue)
{
    qDebug() << "setEncoder2: " << newValue;
    ui->encoder2LabelValue->setText(newValue);
}

void TelemetryTab::setDetectorSwitch1(float newValue)
{
    qDebug() << "setDetectorSwitch1: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case CONT_DETECTED:
        newValueStr = "DETECTED";
        break;
    case CONT_NOT_DETECTED:
        newValueStr = "NOT DETECTED";
        break;
    }

    ui->detectorSwitch1LabelValue->setText(newValueStr);
}

void TelemetryTab::setDetectorSwitch2(float newValue)
{
    qDebug() << "setDetectorSwitch2: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case CONT_DETECTED:
        newValueStr = "DETECTED";
        break;
    case CONT_NOT_DETECTED:
        newValueStr = "NOT DETECTED";
        break;
    }
    ui->detectorSwitch2LabelValue->setText(newValueStr);
}

void TelemetryTab::setDetectorSwitch3(float newValue)
{
    qDebug() << "setDetectorSwitch3: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case CONT_DETECTED:
        newValueStr = "DETECTED";
        break;
    case CONT_NOT_DETECTED:
        newValueStr = "NOT DETECTED";
        break;
    }
    ui->detectorSwitch3LabelValue->setText(newValueStr);
}

void TelemetryTab::setExperimentStatus(float newValue)
{
    qDebug() << "setExperimentStatus: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Experiment_Status::EXP_OK:
        newValueStr = "OK";
        break;
    case Experiment_Status::EXP_ERROR:
        newValueStr = "ERROR";
        break;
    case Experiment_Status::EXP_WARNING:
        newValueStr = "WARNING";
        break;
    }
    ui->experimentStLabelValue->setText(newValueStr);
}

void TelemetryTab::setStateMachine(float newValue)
{
    qDebug() << "setStateMachine: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case State_Machine_Status::PASSIVE:
        newValueStr = "PASSIVE";
        break;
    case State_Machine_Status::MANUAL:
        newValueStr = "MANUAL";
        break;
    case State_Machine_Status::AUTONOMOUS:
        newValueStr = "AUTONOMOUS";
        break;
    }
    ui->statemachineLabelValue->setText(newValueStr);
}

void TelemetryTab::setContainer1Status(float newValue)
{
    qDebug() << "setContainer1Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Container_Status::CONT_OK:
        newValueStr = "OK";
        break;
    case Container_Status::CONT_ERROR:
        newValueStr = "ERROR";
        break;
    case Container_Status::CONT_RELEASED:
        newValueStr = "RELEASED";
        break;
    }
    ui->container1StLabelValue->setText(newValueStr);
}

void TelemetryTab::setContainer2Status(float newValue)
{
    qDebug() << "setContainer2Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Container_Status::CONT_OK:
        newValueStr = "OK";
        break;
    case Container_Status::CONT_ERROR:
        newValueStr = "ERROR";
        break;
    case Container_Status::CONT_RELEASED:
        newValueStr = "RELEASED";
        break;
    }
    ui->container2StLabelValue->setText(newValueStr);
}

void TelemetryTab::setContainer3Status(float newValue)
{
    qDebug() << "setContainer3Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Container_Status::CONT_OK:
        newValueStr = "OK";
        break;
    case Container_Status::CONT_ERROR:
        newValueStr = "ERROR";
        break;
    case Container_Status::CONT_RELEASED:
        newValueStr = "RELEASED";
        break;
    }
    ui->container3StLabelValue->setText(newValueStr);
}

void TelemetryTab::setLastContainerReleased(float newValue)
{
    qDebug() << "setLastContainerReleased: " << newValue;

    QString newValueStr("UNKNOWN");

    if(newValue == 0.0 || newValue == 1.0 || newValue == 2.0 || newValue == 3.0 )
    {
        newValueStr = QString::number((int) newValue);
    }

    ui->lastContReleasedLabelValue->setText(newValueStr);

}

void TelemetryTab::setTemperature1Status(float newValue)
{
    qDebug() << "setTemperature1Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Sensor_Status::SENS_OK:
        newValueStr = "OK";
        break;
    case Sensor_Status::SENS_ERROR:
        newValueStr = "ERROR";
        break;
    }
    ui->tempSensor1StLabelValue->setText(newValueStr);
}

void TelemetryTab::setTemperature2Status(float newValue)
{
    qDebug() << "setTemperature2Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Sensor_Status::SENS_OK:
        newValueStr = "OK";
        break;
    case Sensor_Status::SENS_ERROR:
        newValueStr = "ERROR";
        break;
    }
    ui->tempSensor2StLabelValue->setText(newValueStr);
}

void TelemetryTab::setTemperature3Status(float newValue)
{
    qDebug() << "setTemperature3Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Sensor_Status::SENS_OK:
        newValueStr = "OK";
        break;
    case Sensor_Status::SENS_ERROR:
        newValueStr = "ERROR";
        break;
    }
    ui->tempSensor3StLabelValue->setText(newValueStr);
}

void TelemetryTab::setTemperature4Status(float newValue)
{
    qDebug() << "setTemperature4Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Sensor_Status::SENS_OK:
        newValueStr = "OK";
        break;
    case Sensor_Status::SENS_ERROR:
        newValueStr = "ERROR";
        break;
    }
    ui->tempSensor4StLabelValue->setText(newValueStr);
}

void TelemetryTab::setTemperature5Status(float newValue)
{
    qDebug() << "setTemperature5Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Sensor_Status::SENS_OK:
        newValueStr = "OK";
        break;
    case Sensor_Status::SENS_ERROR:
        newValueStr = "ERROR";
        break;
    }
    ui->tempSensor5StLabelValue->setText(newValueStr);
}

void TelemetryTab::setTemperature6Status(float newValue)
{
    qDebug() << "setTemperature6Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Sensor_Status::SENS_OK:
        newValueStr = "OK";
        break;
    case Sensor_Status::SENS_ERROR:
        newValueStr = "ERROR";
        break;
    }
    ui->tempSensor6StLabelValue->setText(newValueStr);
}

void TelemetryTab::setHumidityStatus(float newValue)
{
    qDebug() << "setHumidityStatus: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Sensor_Status::SENS_OK:
        newValueStr = "OK";
        break;
    case Sensor_Status::SENS_ERROR:
        newValueStr = "ERROR";
        break;
    }
    ui->humSensorStLabelValue->setText(newValueStr);
}

void TelemetryTab::setGPSStatus(float newValue)
{
    qDebug() << "setGPSStatus: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Sensor_Status::SENS_OK:
        newValueStr = "OK";
        break;
    case Sensor_Status::SENS_ERROR:
        newValueStr = "ERROR";
        break;
    }
    ui->gpsSensorStLabelValue->setText(newValueStr);
}

void TelemetryTab::setPressureStatus(float newValue)
{
    qDebug() << "setPressureStatus: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Sensor_Status::SENS_OK:
        newValueStr = "OK";
        break;
    case Sensor_Status::SENS_ERROR:
        newValueStr = "ERROR";
        break;
    }
    ui->pressSensorStLabelValue->setText(newValueStr);
}

void TelemetryTab::setIMUStatus(float newValue)
{
    qDebug() << "setIMUStatus: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Sensor_Status::SENS_OK:
        newValueStr = "OK";
        break;
    case Sensor_Status::SENS_ERROR:
        newValueStr = "ERROR";
        break;
    }
    ui->imuStLabelValue->setText(newValueStr);
}

void TelemetryTab::setMotor1Status(float newValue)
{
    qDebug() << "setMotor1Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Actuator_Status::ACT_ON:
        newValueStr = "ON";
        break;
    case Actuator_Status::ACT_OFF:
        newValueStr = "OFF";
        break;
    }
    ui->motor1StLabelValue->setText(newValueStr);
}

void TelemetryTab::setMotor2Status(float newValue)
{
    qDebug() << "setMotor2Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Actuator_Status::ACT_ON:
        newValueStr = "ON";
        break;
    case Actuator_Status::ACT_OFF:
        newValueStr = "OFF";
        break;
    }
    ui->motor2StLabelValue->setText(newValueStr);
}

void TelemetryTab::setServo1Status(float newValue)
{
    qDebug() << "setServo1Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Actuator_Status::ACT_ON:
        newValueStr = "ON";
        break;
    case Actuator_Status::ACT_OFF:
        newValueStr = "OFF";
        break;
    }
    ui->servo1StLabelValue->setText(newValueStr);
}

void TelemetryTab::setServo2Status(float newValue)
{
    qDebug() << "setServo2Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Actuator_Status::ACT_ON:
        newValueStr = "ON";
        break;
    case Actuator_Status::ACT_OFF:
        newValueStr = "OFF";
        break;
    }
    ui->servo2StLabelValue->setText(newValueStr);
}

void TelemetryTab::setServo3Status(float newValue)
{
    qDebug() << "setServo3Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Actuator_Status::ACT_ON:
        newValueStr = "ON";
        break;
    case Actuator_Status::ACT_OFF:
        newValueStr = "OFF";
        break;
    }
    ui->servo3StLabelValue->setText(newValueStr);
}

void TelemetryTab::setCameraSystemStatus(float newValue)
{
    qDebug() << "setCameraSystemStatus: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Camera_Status::CAM_ON:
        newValueStr = "ON";
        break;
    case Camera_Status::CAM_OFF:
        newValueStr = "OFF";
        break;
    case Camera_Status::CAM_RECORDING:
        newValueStr = "RECORDING";
        break;
    }
    ui->cameraSystemStLabelValue->setText(newValueStr);
}

void TelemetryTab::setHeater1Status(float newValue)
{
    qDebug() << "setHeater1Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Actuator_Status::ACT_ON:
        newValueStr = "ON";
        break;
    case Actuator_Status::ACT_OFF:
        newValueStr = "OFF";
        break;
    }
    ui->heater1StLabelValue->setText(newValueStr);
}

void TelemetryTab::setHeater2Status(float newValue)
{
    qDebug() << "setHeater2Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Actuator_Status::ACT_ON:
        newValueStr = "ON";
        break;
    case Actuator_Status::ACT_OFF:
        newValueStr = "OFF";
        break;
    }
    ui->heater2StLabelValue->setText(newValueStr);
}

void TelemetryTab::setHeater3Status(float newValue)
{
    qDebug() << "setHeater3Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Actuator_Status::ACT_ON:
        newValueStr = "ON";
        break;
    case Actuator_Status::ACT_OFF:
        newValueStr = "OFF";
        break;
    }
    ui->heater3StLabelValue->setText(newValueStr);
}

void TelemetryTab::setHeater4Status(float newValue)
{
    qDebug() << "setHeater4Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Actuator_Status::ACT_ON:
        newValueStr = "ON";
        break;
    case Actuator_Status::ACT_OFF:
        newValueStr = "OFF";
        break;
    }
    ui->heater4StLabelValue->setText(newValueStr);
}

void TelemetryTab::setHeater5Status(float newValue)
{
    qDebug() << "setHeater5Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Actuator_Status::ACT_ON:
        newValueStr = "ON";
        break;
    case Actuator_Status::ACT_OFF:
        newValueStr = "OFF";
        break;
    }
    ui->heater5StLabelValue->setText(newValueStr);
}

void TelemetryTab::setTimeLastContainerReleased(float newValue)
{
    qDebug() << "setTimeLastContainerReleased: " << newValue;

    QString newValueStr("UNKNOWN");
    int newValueInt = (int)newValue;
    newValueStr = QString::number(newValueInt / 1000000) + ":" +
                  QString::number((newValueInt / 10000) % 100) + ":"   +
                  QString::number((newValueInt / 100) % 100)   /*+ ":"   +
                  QString::number(newValueInt % 100)*/;

    ui->timeLastContainerReleaseLabelValue->setText(newValueStr);
}
void TelemetryTab::setReleaseGapTime(float newValue)
{
    qDebug() << "setTimeLastContainerReleased: " << newValue;

    QString newValueStr("UNKNOWN");
    int newValueInt = (int)newValue;
    newValueStr = QString::number(newValueInt);

    ui->releaseGapLabelValue->setText(newValueStr);
}
