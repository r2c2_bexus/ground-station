#include "locationtab.h"
#include "ui_locationTab.h"

#include <QDebug>
#include "telemetryVariables.h"

#define PLOTSIZE       1800

LocationTab::LocationTab(QWidget *parent) :
    R2C2Tab(parent),
    ui(new Ui::LocationTab)
{
    ui->setupUi(this);
    this->setWindowTitle("R2C2 Ground Station");

    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%h:%m:%s");

    ui->altitudePlot->addGraph();
    ui->altitudePlot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->altitudePlot->xAxis->setLabel("Time");
    ui->altitudePlot->yAxis->setLabel("Altitude");
    ui->altitudePlot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->altitudePlot->axisRect()->setupFullAxesBox();
    ui->altitudePlot->xAxis->setRange(0, PLOTSIZE);
    ui->altitudePlot->xAxis->setTicker(timeTicker);
    ui->altitudePlot->replot();

    ui->positionPlot->addGraph();
    ui->positionPlot->xAxis->setLabel("Latitude");
    ui->positionPlot->yAxis->setLabel("Longitude");
    ui->positionPlot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->positionPlot->axisRect()->setupFullAxesBox();
    ui->positionPlot->xAxis->setRange(0, 0.001);
    ui->positionPlot->setInteraction(QCP::iRangeDrag, true);
    ui->positionPlot->replot();

}



void LocationTab::setValue(unsigned int valueType, float newValue)
{
    QString value;

    static const float errorValue = 0.0;
    static float longitudeValue = errorValue;
    static float latitudeValue = errorValue;
    static bool newLongitude = false;
    static bool newLatitude = false;

    switch (valueType) {
    case GPS_LONGITUDE_ID:
        longitudeValue = newValue;
        if(longitudeValue != errorValue)
        {
            newLongitude = true;
        }
        break;
    case GPS_LATITUDE_ID:
        latitudeValue = newValue;
        if(latitudeValue != errorValue)
        {
            newLatitude = true;
        }
        break;
    case GPS_ALTITUDE_ID:
        setAltitude(newValue);
        break;
    }

    if(newLongitude && newLatitude)
    {
        setPosition(latitudeValue, longitudeValue);
        newLongitude = false;
        newLatitude = false;
    }
}


void LocationTab::setPosition(float newLatitudeValue, float newLongitudeValue)
{
    QString value;
    value.setNum(newLongitudeValue, 'f', 10);
    ui->longitudeLabelValue->setText(value);
    value.setNum(newLatitudeValue, 'f', 10);
    ui->latitudeLabelValue->setText(value);

    qDebug() << "setPosition: Latitude: " << newLatitudeValue
                          << "Longitude: " << newLongitudeValue;

    ui->positionPlot->graph(0)->addData(newLatitudeValue, newLongitudeValue);
    /*if (ui->positionPlot->graph(0)->dataCount() > 10)
    {
        ui->positionPlot->graph(0)->data()->
    }*/
    // rescale value (vertical) axis to fit the current data:
    ui->positionPlot->graph(0)->rescaleValueAxis(true, true);
    // make key axis range scroll with the data (at a constant range size of 8):
    ui->positionPlot->xAxis->setRange(newLatitudeValue, 0.001, Qt::AlignCenter);
    ui->positionPlot->yAxis->setRange(newLongitudeValue, 0.001, Qt::AlignCenter);
    ui->positionPlot->replot();

}


void LocationTab::setAltitude(float newValue)
{
    QString value;
    value.setNum(newValue);
    ui->altitudeLabelValue->setText(value);

    qDebug() << "setAltitude: " << newValue;

    // calculate two new data points:
    static QTime time(QTime::currentTime());
    // calculate two new data points:
    double key = time.elapsed()/1000.0;
    //double key = timeLastMessageReceived./1000.0;
    // add data to lines:
    ui->altitudePlot->graph(0)->addData(key, newValue);
    // rescale value (vertical) axis to fit the current data:
    ui->altitudePlot->graph(0)->rescaleValueAxis(true);
    // make key axis range scroll with the data (at a constant range size of 8):
    ui->altitudePlot->xAxis->setRange(key, PLOTSIZE, Qt::AlignRight);
    ui->altitudePlot->replot();
}
