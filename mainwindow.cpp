#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("R2C2 Ground Station");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setTemperature1(int newValue)
{
    qDebug() << "setTemperature1: " << newValue;
    ui->temp1LineEdit->setText(QString::number(newValue));
}

void MainWindow::setTemperature2(int newValue)
{
    qDebug() << "setTemperature2: " << newValue;
    ui->temp2LineEdit->setText(QString::number(newValue));
}

void MainWindow::setTemperature3(int newValue)
{
    qDebug() << "setTemperature3: " << newValue;
    ui->temp3LineEdit->setText(QString::number(newValue));
}

void MainWindow::setTemperature4(int newValue)
{
    qDebug() << "setTemperature4: " << newValue;
    ui->temp4LineEdit->setText(QString::number(newValue));
}

void MainWindow::setTemperature5(int newValue)
{
    qDebug() << "setTemperature5: " << newValue;
    ui->temp5LineEdit->setText(QString::number(newValue));
}

void MainWindow::setTemperature6(int newValue)
{
    qDebug() << "setTemperature6: " << newValue;
    ui->temp6LineEdit->setText(QString::number(newValue));
}

void MainWindow::setHumidity(unsigned int newValue)
{
    qDebug() << "setHumidity: " << newValue;
    ui->humidityLabelValue->setText(QString::number(newValue));
}

void MainWindow::setLongitude(float newValue)
{
    qDebug() << "setLongitude: " << newValue;
    ui->longitudeLabelValue->setText(QString::number(newValue));
}

void MainWindow::setLatitude(float newValue)
{
    qDebug() << "setLatitude: " << newValue;
    ui->latitudeLabelValue->setText(QString::number(newValue));
}

void MainWindow::setAltitude(unsigned int newValue)
{
    qDebug() << "setAltitude: " << newValue;
    ui->altitudeLabelValue->setText(QString::number(newValue));
}

void MainWindow::setPressure(unsigned int newValue)
{
    qDebug() << "setPressure: " << newValue;
    ui->pressureLabelValue->setText(QString::number(newValue));
}

void MainWindow::on_setPassiveModeButton_clicked()
{
    emit sendCommandToExperiment(CMD_PASSIVE_MODE);
}

void MainWindow::on_setManualModeButton_clicked()
{
    emit sendCommandToExperiment(CMD_MANUAL_MODE);
}

void MainWindow::on_setActiveModeButton_clicked()
{
    emit sendCommandToExperiment(CMD_ACTIVE_MODE);
}

void MainWindow::on_setAutonomousModeButton_clicked()
{
    emit sendCommandToExperiment(CMD_AUTONOMOUS_MODE);
}

void MainWindow::on_releaseContainer1Button_clicked()
{
    emit sendCommandToExperiment(CMD_RELEASE_CONTAINER_1);
}

void MainWindow::on_releaseContainer2Button_clicked()
{
    emit sendCommandToExperiment(CMD_RELEASE_CONTAINER_2);
}

void MainWindow::on_releaseContainer3Button_clicked()
{
    emit sendCommandToExperiment(CMD_RELEASE_CONTAINER_3);
}

void MainWindow::on_activateMotor1Button_clicked()
{
    emit sendCommandToExperiment(CMD_ACTIVATE_MOTOR_1);
}

void MainWindow::on_activateMotor2Button_clicked()
{
    emit sendCommandToExperiment(CMD_ACTIVATE_MOTOR_2);
}

void MainWindow::on_deactivateMotor1Button_clicked()
{
    emit sendCommandToExperiment(CMD_DEACTIVATE_MOTOR_1);
}

void MainWindow::on_deactivateMotor2Button_clicked()
{
    emit sendCommandToExperiment(CMD_DEACTIVATE_MOTOR_2);
}

void MainWindow::on_activateHeater1Button_clicked()
{
    emit sendCommandToExperiment(CMD_ACTIVATE_HEATER_1);
}

void MainWindow::on_activateHeater2Button_clicked()
{
    emit sendCommandToExperiment(CMD_ACTIVATE_HEATER_2);
}

void MainWindow::on_activateHeater3Button_clicked()
{
    emit sendCommandToExperiment(CMD_ACTIVATE_HEATER_3);
}

void MainWindow::on_activateHeater4Button_clicked()
{
    emit sendCommandToExperiment(CMD_ACTIVATE_HEATER_4);
}

void MainWindow::on_activateHeater5Button_clicked()
{
    emit sendCommandToExperiment(CMD_ACTIVATE_HEATER_5);
}

void MainWindow::on_deactivateHeater1Button_clicked()
{
    emit sendCommandToExperiment(CMD_DEACTIVATE_HEATER_1);
}

void MainWindow::on_deactivateHeater2Button_clicked()
{
    emit sendCommandToExperiment(CMD_DEACTIVATE_HEATER_2);
}

void MainWindow::on_deactivateHeater3Button_clicked()
{
    emit sendCommandToExperiment(CMD_DEACTIVATE_HEATER_3);
}

void MainWindow::on_deactivateHeater4Button_clicked()
{
    emit sendCommandToExperiment(CMD_DEACTIVATE_HEATER_4);
}

void MainWindow::on_deactivateHeater5Button_clicked()
{
    emit sendCommandToExperiment(CMD_DEACTIVATE_HEATER_5);
}

void MainWindow::on_cameraSystemOnButton_clicked()
{
    emit sendCommandToExperiment(CMD_CAMERA_MANAGER_ON);
}

void MainWindow::on_cameraSystemOffButton_clicked()
{
    emit sendCommandToExperiment(CMD_CAMERA_MANAGER_OFF);
}
