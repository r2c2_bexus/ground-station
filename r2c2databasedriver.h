#ifndef R2C2DATABASEDRIVER_H
#define R2C2DATABASEDRIVER_H

#include <QObject>
#include <QSqlDatabase>

class R2C2DatabaseDriver : public QObject
{
    Q_OBJECT
public:
    explicit R2C2DatabaseDriver(QObject *parent = nullptr);

    ~R2C2DatabaseDriver();

signals:

public slots:

private:

    QSqlDatabase r2c2DB;
};

#endif // R2C2DATABASEDRIVER_H
