#ifndef SENSORS_H
#define SENSORS_H

#define TEMPERATURE_SENS_1_ID   1
#define TEMPERATURE_SENS_2_ID   2
#define TEMPERATURE_SENS_3_ID   3
#define TEMPERATURE_SENS_4_ID   4
#define TEMPERATURE_SENS_5_ID   5
#define TEMPERATURE_SENS_6_ID   6
#define HUMIDITY_SENS_ID        7
#define GPS_LATITUDE_ID         8
#define GPS_LONGITUDE_ID        9
#define GPS_ALTITUDE_ID        10
#define PRESSURE_SENS_ID       11
#define DETECTOR_SWITCH_SENS_1_ID       12
#define DETECTOR_SWITCH_SENS_2_ID       13
#define DETECTOR_SWITCH_SENS_3_ID       14
#define ENCODER_1_PULSES_ID             15
#define ENCODER_2_PULSES_ID             16

#define EXPERIMENT_STATUS_ID        20
#define STATE_MACHINE_ID            21
#define CONTAINER_1_STATUS_ID       22
#define CONTAINER_2_STATUS_ID       23
#define CONTAINER_3_STATUS_ID       24
#define LAST_CONTAINER_RELEASED_ID  25
#define TEMPERATURE_1_STATUS_ID     26
#define TEMPERATURE_2_STATUS_ID     27
#define TEMPERATURE_3_STATUS_ID     28
#define TEMPERATURE_4_STATUS_ID     29
#define TEMPERATURE_5_STATUS_ID     30
#define TEMPERATURE_6_STATUS_ID     31
#define HUMIDITY_STATUS_ID          32
#define GPS_STATUS_ID               33
#define PRESSURE_STATUS_ID          34
#define MOTOR_1_STATUS_ID           35
#define MOTOR_2_STATUS_ID           36
#define SERVO_1_STATUS_ID           37
#define SERVO_2_STATUS_ID           38
#define SERVO_3_STATUS_ID           39
#define CAMERA_STATUS_ID            40
#define HEATER_1_STATUS_ID          41
#define HEATER_2_STATUS_ID          42
#define HEATER_3_STATUS_ID          43
#define HEATER_4_STATUS_ID          44
#define HEATER_5_STATUS_ID          45
#define TIME_LAST_CONTAINER_RELEASED_ID   46
#define RELEASE_GAP_TIME_ID         47
#define IMU_STATUS_ID               48
#define COMM_CONNECTION_ID          49

#define COMM_LASTMESSAGERECEIVED    101

#define CONT_DETECTED                0
#define CONT_NOT_DETECTED            1

#define STATUS_MESSAGE               0
#define SENSOR_MESSAGE               1


#endif // SENSORS_H
