#-------------------------------------------------
#
# Project created by QtCreator 2019-04-22T17:58:12
#
#-------------------------------------------------

QT       += core gui
QT       += network
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = R2C2GroundStation
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS QT_DEBUG_PLUGINS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        connector.cpp \
        generalTab.cpp \
        locationtab.cpp \
        main.cpp \
        qcustomplot.cpp \
        r2c2communicationnode.cpp \
        r2c2databasedriver.cpp \
        r2c2filedriver.cpp \
        r2c2tab.cpp \
        telemetryTab.cpp

HEADERS += \
        commands.h \
        connector.h \
        generalTab.h \
        locationtab.h \
        qcustomplot.h \
        r2c2communicationnode.h \
        r2c2databasedriver.h \
        r2c2filedriver.h \
        r2c2tab.h \
        status.h \
        telemetryTab.h \
        telemetryVariables.h

FORMS += \
        generalTab.ui \
        locationTab.ui \
        telemetryTab.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    r2c2_logo.png
