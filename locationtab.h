#ifndef LOCATIONTAB_H
#define LOCATIONTAB_H

#include "r2c2tab.h"
#include "qcustomplot.h"

namespace Ui {
class LocationTab;
}


class LocationTab : public R2C2Tab
{
    Q_OBJECT

public:
    LocationTab(QWidget *parent = nullptr);

public slots:
    void setValue(unsigned int valueType, float newValue);

private:

    void setPosition(float newLatitudeValue, float newLongitudeValue);
    void setAltitude(float newValue);

    Ui::LocationTab *ui;
};

#endif // LOCATIONTAB_H
