#ifndef R2C2COMMUNICATIONNODE_H
#define R2C2COMMUNICATIONNODE_H

#include <QUdpSocket>
#include <QTcpSocket>
#include <QNetworkDatagram>
#include <QTimer>
#include <QTime>

#include <QObject>

class R2C2CommunicationNode: public QObject
{
    Q_OBJECT
public:
    R2C2CommunicationNode();

signals:
    void valueChanged(unsigned int typeValue, float newValue);
    void dataReceived(unsigned int typeData, QByteArray data);

public slots:
    void sendCommand(char command);
    void sendCommandWithVariable(char command, uint16_t value);

private slots:
    void sendPing();
    void checkConnection();

private:
    void initSockets();
    void startPing();
    void readPendingDatagrams();
    void processDatagram(QNetworkDatagram& datagram);

    void processSensorData(QByteArray& datagram);
    void processStatusData(QByteArray& datagram);

    void processHeader(QByteArray& header);

    QUdpSocket* udpSocket;
    QTcpSocket* tcpSocket;

    bool commandingConnected;
    QTime timeLastMessageReceived;

    QTimer* pingTimer;
    QTimer* connectionTimer;
};

#endif // R2C2COMMUNICATIONNODE_H
