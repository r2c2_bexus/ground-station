#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "commands.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void sendCommandToExperiment(unsigned int mode);

public slots:
    void setTemperature1(int newValue);
    void setTemperature2(int newValue);
    void setTemperature3(int newValue);
    void setTemperature4(int newValue);
    void setTemperature5(int newValue);
    void setTemperature6(int newValue);

    void setHumidity(unsigned int newValue);
    void setLongitude(float newValue);
    void setLatitude(float newValue);
    void setAltitude(unsigned int newValue);
    void setPressure(unsigned int newValue);

private slots:
    void on_setPassiveModeButton_clicked();

    void on_setManualModeButton_clicked();

    void on_setActiveModeButton_clicked();

    void on_setAutonomousModeButton_clicked();

    void on_releaseContainer1Button_clicked();

    void on_releaseContainer2Button_clicked();

    void on_releaseContainer3Button_clicked();

    void on_activateMotor1Button_clicked();

    void on_activateMotor2Button_clicked();

    void on_deactivateMotor1Button_clicked();

    void on_deactivateMotor2Button_clicked();

    void on_activateHeater1Button_clicked();

    void on_activateHeater2Button_clicked();

    void on_activateHeater3Button_clicked();

    void on_activateHeater4Button_clicked();

    void on_activateHeater5Button_clicked();

    void on_deactivateHeater1Button_clicked();

    void on_deactivateHeater2Button_clicked();

    void on_deactivateHeater3Button_clicked();

    void on_deactivateHeater4Button_clicked();

    void on_deactivateHeater5Button_clicked();

    void on_cameraSystemOnButton_clicked();

    void on_cameraSystemOffButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
