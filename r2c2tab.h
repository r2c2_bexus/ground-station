#ifndef R2C2TAB_H
#define R2C2TAB_H

#include <QWidget>

class R2C2Tab : public QWidget
{
    Q_OBJECT

public:
    R2C2Tab(QWidget *parent = nullptr);

signals:
    void sendCommand(char mode);
    void sendCommandWithVariable(char mode, uint16_t value);

public slots:
    virtual void setValue(unsigned int valueType, float newValue) = 0;
};

#endif // R2C2TAB_H
