#ifndef CONFIG_H
#define CONFIG_H

/*#define STATE_MACHINE_PASSIVE_MODE               0x00
#define STATE_MACHINE_MANUAL_MODE                0x01
#define STATE_MACHINE_AUTONOMOUS_MODE            0x02*/

enum Experiment_Status 		 {EXP_OK = 0, EXP_ERROR, EXP_WARNING};
enum Container_Status 		 {CONT_OK = 0, CONT_ERROR, CONT_RELEASED};
enum Sensor_Status 		     {SENS_OK = 0, SENS_ERROR};
enum State_Machine_Status 	 {PASSIVE = 0, MANUAL, AUTONOMOUS};
enum Actuator_Status 		 {ACT_ON = 0, ACT_OFF};
enum Camera_Status 		     {CAM_ON = 0, CAM_OFF, CAM_RECORDING};
enum Communication_Status    {COM_CONNECTED = 0, COM_DISCONNECTED};

#endif // CONFIG_H
