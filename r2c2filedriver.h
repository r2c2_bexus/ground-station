#ifndef R2C2FILEDRIVER_H
#define R2C2FILEDRIVER_H

#include <QObject>
#include <QFile>
#include <QTextStream>

class R2C2FileDriver:  public QObject
{
    Q_OBJECT
public:
    R2C2FileDriver(QString filename, QObject *parent = nullptr);
    ~R2C2FileDriver();

public slots:
    void writeData(unsigned int typeData, QByteArray data);

private:

    QFile file;
    bool isFileOpen;

    void writeStatusData(QByteArray message);
    void writeSensorData(QByteArray message);
};

#endif // R2C2FILEDRIVER_H
