#include "generalview.h"
#include "ui_generalview.h"

#include <QChart>
#include <QChartView>
#include <QLineSeries>

#include <QDebug>

GeneralView::GeneralView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GeneralView)
{
    ui->setupUi(this);
    this->setWindowTitle("R2C2 Ground Station");

    //![1]
        QLineSeries *series = new QLineSeries();
        QLineSeries *series2 = new QLineSeries();
        QLineSeries *series3 = new QLineSeries();
        QLineSeries *series4 = new QLineSeries();
        QLineSeries *series5 = new QLineSeries();
        QLineSeries *series6 = new QLineSeries();
    //![1]

    //![2]
        //QDateTime timeNow;
        //series->append(timeNow.currentDateTime(), 6);
        series->append(2, 4);
        series->append(3, 8);
        series->append(7, 4);
        series->append(10, 5);
        *series << QPointF(11, 1) << QPointF(13, 3) << QPointF(17, 6) << QPointF(18, 3) << QPointF(20, 2);

        series2->append(2, 4);
        series2->append(3, 18);
        series2->append(5, 9);
        series2->append(10, 15);

        series3->append(2, 24);
        series3->append(4, 15);
        series3->append(6, 61);
        series3->append(8, 47);

        series4->append(2, 1);
        series4->append(3, 8);
        series4->append(7, 14);
        series4->append(10, 25);

        series5->append(21, 210);
        series5->append(38, 35);
        series5->append(73, 70);
        series5->append(100, 105);

        series6->append(3, 2);
        series6->append(8, 4);
        series6->append(10, 6);
        series6->append(15, 8);
    //![2]

    //![3]
        QChart *chart = new QChart();
        chart->legend()->hide();
        chart->addSeries(series);
        chart->createDefaultAxes();
        chart->setTitle("Temperature");

        /*QDateTimeAxis *axisX = new QDateTimeAxis;
        axisX->setTickCount(10);
        axisX->setFormat("hh:mm:ss");
        axisX->setTitleText("Time");
        chart->addAxis(axisX, Qt::AlignBottom);
        series->attachAxis(axisX);*/

        QChart *chart2 = new QChart();
        chart2->legend()->hide();
        chart2->addSeries(series2);
        chart2->createDefaultAxes();
        chart2->setTitle("Temperature");

        QChart *chart3 = new QChart();
        chart3->legend()->hide();
        chart3->addSeries(series3);
        chart3->createDefaultAxes();
        chart3->setTitle("Temperature");

        QChart *chart4 = new QChart();
        chart4->legend()->hide();
        chart4->addSeries(series4);
        chart4->createDefaultAxes();
        chart4->setTitle("Temperature");

        QChart *chart5 = new QChart();
        chart5->legend()->hide();
        chart5->addSeries(series5);
        chart5->createDefaultAxes();
        chart5->setTitle("Temperature");

        QChart *chart6 = new QChart();
        chart6->legend()->hide();
        chart6->addSeries(series6);
        chart6->createDefaultAxes();
        chart6->setTitle("Temperature");

        ui->testChart->setChart(chart);
        ui->testChart_2->setChart(chart2);
        ui->testChart_3->setChart(chart3);
        ui->testChart_4->setChart(chart4);
        ui->testChart_5->setChart(chart5);
        ui->testChart_6->setChart(chart6);
}

GeneralView::~GeneralView()
{
    delete ui;
}
