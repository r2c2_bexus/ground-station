#include "telemetryTab.h"
#include "generalTab.h"
#include "locationtab.h"

//#include "r2c2databasedriver.h"
#include "r2c2communicationnode.h"
#include "connector.h"
#include "r2c2filedriver.h"

#include <QObject>
#include <QApplication>
#include <QWidget>
#include <QMainWindow>
#include <QTabWidget>


int main(int argc, char *argv[])
{
    int width = 1700;
    int heigth = 1000;

    QApplication a(argc, argv);
    QMainWindow *window = new QMainWindow();
    window->resize(width, heigth);
    QIcon r2c2Icon("../R2C2GroundStation/r2c2_logo");
    window->setWindowIcon(r2c2Icon);

    QWidget *centralWidget = new QWidget(window);
    QTabWidget *tabs = new QTabWidget(centralWidget);

    QMainWindow *window2 = new QMainWindow();
    window2->resize(width, heigth);
    window2->setWindowIcon(r2c2Icon);

    QWidget *centralWidget2 = new QWidget(window2);
    QTabWidget *tabs2 = new QTabWidget(centralWidget2);

    GeneralTab   generalTab;
    R2C2Tab* pGeneralTab = &generalTab;

    LocationTab locationTab;
    R2C2Tab* pLocationTab = &locationTab;

    TelemetryTab telemetryTab;
    R2C2Tab* pTelemetryTab = &telemetryTab;

    tabs->resize(width, heigth);
    tabs2->resize(width, heigth);
    tabs->addTab(pGeneralTab, "General");
    tabs->addTab(pLocationTab, "Location");
    //tabs->addTab(pTelemetryTab, "Telemetry");
    tabs2->addTab(pTelemetryTab, "Telemetry");

    window->setCentralWidget(centralWidget);
    window2->setCentralWidget(centralWidget2);

    window->show();
    window2->show();

    //R2C2DatabaseDriver dbDriver;
    QString dateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss");
    QString fileName = "FligthExperimentDataLog-" + dateTime + ".txt";
    R2C2FileDriver fileDriver(fileName);

    R2C2CommunicationNode commNode;

    Connector* connector = new Connector();
    connector->addDataSubscriber(generalTab, commNode);
    connector->addDataSubscriber(telemetryTab, commNode);
    connector->addDataSubscriber(locationTab, commNode);
    connector->addCommandPublisher(generalTab, commNode);
    connector->addMessageSubscriber(fileDriver, commNode);

    return a.exec();
}
