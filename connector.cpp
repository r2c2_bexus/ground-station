#include "connector.h"

void Connector::addDataSubscriber(R2C2Tab &tab, R2C2CommunicationNode &commNode)
{
    connect(&commNode, &R2C2CommunicationNode::valueChanged, &tab, &R2C2Tab::setValue);
}

void Connector::addCommandPublisher(R2C2Tab &tab, R2C2CommunicationNode &commNode)
{
    connect(&tab, &R2C2Tab::sendCommand, &commNode, &R2C2CommunicationNode::sendCommand);
    connect(&tab, &R2C2Tab::sendCommandWithVariable, &commNode, &R2C2CommunicationNode::sendCommandWithVariable);
}

void Connector::addMessageSubscriber(R2C2FileDriver &fileDriver, R2C2CommunicationNode &commNode)
{
    connect(&commNode, &R2C2CommunicationNode::dataReceived, &fileDriver, &R2C2FileDriver::writeData);
}
