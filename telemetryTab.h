#ifndef TELEMETRYTAB_H
#define TELEMETRYTAB_H

#include "r2c2tab.h"

namespace Ui {
class TelemetryTab;
}

class TelemetryTab : public R2C2Tab
{
    Q_OBJECT

public:
    explicit TelemetryTab(QWidget *parent = nullptr);
    ~TelemetryTab();

public slots:
    void setValue(unsigned int valueType, float newValue);

private:
    void setTemperature1(QString newValue);
    void setTemperature2(QString newValue);
    void setTemperature3(QString newValue);
    void setTemperature4(QString newValue);
    void setTemperature5(QString newValue);
    void setTemperature6(QString newValue);

    void setHumidity(QString newValue);
    void setLongitude(QString newValue);
    void setLatitude(QString newValue);
    void setAltitude(QString newValue);
    void setPressure(QString newValue);

    void setEncoder1(QString newValue);
    void setEncoder2(QString newValue);

    void setDetectorSwitch1(float newValue);
    void setDetectorSwitch2(float newValue);
    void setDetectorSwitch3(float newValue);

    void setExperimentStatus(float newValue);
    void setStateMachine(float newValue);
    void setContainer1Status(float newValue);
    void setContainer2Status(float newValue);
    void setContainer3Status(float newValue);
    void setLastContainerReleased(float newValue);

    void setTemperature1Status(float newValue);
    void setTemperature2Status(float newValue);
    void setTemperature3Status(float newValue);
    void setTemperature4Status(float newValue);
    void setTemperature5Status(float newValue);
    void setTemperature6Status(float newValue);
    void setHumidityStatus(float newValue);
    void setGPSStatus(float newValue);
    void setPressureStatus(float newValue);
    void setIMUStatus(float newValue);

    void setMotor1Status(float newValue);
    void setMotor2Status(float newValue);
    void setServo1Status(float newValue);
    void setServo2Status(float newValue);
    void setServo3Status(float newValue);
    void setCameraSystemStatus(float newValue);
    void setHeater1Status(float newValue);
    void setHeater2Status(float newValue);
    void setHeater3Status(float newValue);
    void setHeater4Status(float newValue);
    void setHeater5Status(float newValue);

    void setTimeLastContainerReleased(float newValue);
    void setReleaseGapTime(float newValue);

    Ui::TelemetryTab *ui;
};

#endif // TELEMETRYTAB_H
