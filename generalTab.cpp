#include "generalTab.h"
#include "ui_generalTab.h"

#include <QDebug>
#include "commands.h"
#include "telemetryVariables.h"
#include "status.h"

#define PLOTSIZE       1800

GeneralTab::GeneralTab(QWidget *parent) :
    R2C2Tab(parent),
    ui(new Ui::GeneralTab)
{
    ui->setupUi(this);
    this->setWindowTitle("R2C2 Ground Station");

    QPixmap r2c2Logo("../R2C2GroundStation/r2c2_logo.png");
    ui->r2c2ImageLabel->setPixmap(r2c2Logo);

    int logoWidth = 400;
    int logoHeigth = ui->r2c2ImageLabel->heightForWidth(logoWidth);
    ui->r2c2ImageLabel->resize(logoWidth, logoHeigth);

    timeLastMessageReceived.fromString("0:0:0", "%h:%m:%s");

    /*for (int i = PLOTSIZE -1; i >= 0; i--) {
        temp1Vector.push_back(i);
        temp2Vector.push_back(0.0);
        temp3Vector.push_back(0.0);
        temp4Vector.push_back(0.0);
        temp5Vector.push_back(0.0);
        temp6Vector.push_back(0.0);

        temp1TimeVector.push_back(i);
        temp2TimeVector.push_back(-i);
        temp3TimeVector.push_back(-i);
        temp4TimeVector.push_back(-i);
        temp5TimeVector.push_back(-i);
        temp6TimeVector.push_back(-i);
    }*/

    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%h:%m:%s");

    ui->temp1Plot->addGraph();
    ui->temp1Plot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->temp1Plot->xAxis->setLabel("Time");
    ui->temp1Plot->yAxis->setLabel("Temperature (M1) 1");
    ui->temp1Plot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->temp1Plot->axisRect()->setupFullAxesBox();
    ui->temp1Plot->xAxis->setRange(0, PLOTSIZE);
    ui->temp1Plot->xAxis->setTicker(timeTicker);
    ui->temp1Plot->replot();

    ui->temp2Plot->addGraph();
    ui->temp2Plot->xAxis->setLabel("Time");
    ui->temp2Plot->yAxis->setLabel("Temperature (M2) 2");
    ui->temp2Plot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->temp2Plot->axisRect()->setupFullAxesBox();
    ui->temp2Plot->xAxis->setRange(0, PLOTSIZE);
    ui->temp2Plot->xAxis->setTicker(timeTicker);
    ui->temp2Plot->replot();

    ui->temp3Plot->addGraph();
    ui->temp3Plot->xAxis->setLabel("Time");
    ui->temp3Plot->yAxis->setLabel("Temperature (E. Box) 3");
    ui->temp3Plot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->temp3Plot->axisRect()->setupFullAxesBox();
    ui->temp3Plot->xAxis->setRange(0, PLOTSIZE);
    ui->temp3Plot->xAxis->setTicker(timeTicker);
    ui->temp3Plot->replot();

    ui->temp4Plot->addGraph();
    ui->temp4Plot->xAxis->setLabel("Time");
    ui->temp4Plot->yAxis->setLabel("Temperature (Camera) 4");
    ui->temp4Plot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->temp4Plot->axisRect()->setupFullAxesBox();
    ui->temp4Plot->xAxis->setRange(0, PLOTSIZE);
    ui->temp4Plot->xAxis->setTicker(timeTicker);
    ui->temp4Plot->replot();

    ui->temp5Plot->addGraph();
    ui->temp5Plot->xAxis->setLabel("Time");
    ui->temp5Plot->yAxis->setLabel("Temperature (Servos) 5");
    ui->temp5Plot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->temp5Plot->axisRect()->setupFullAxesBox();
    ui->temp5Plot->xAxis->setRange(0, PLOTSIZE);
    ui->temp5Plot->xAxis->setTicker(timeTicker);
    ui->temp5Plot->replot();

    ui->temp6Plot->addGraph();
    ui->temp6Plot->xAxis->setLabel("Time");
    ui->temp6Plot->yAxis->setLabel("Temperature 6");
    ui->temp6Plot->graph(0)->setPen(QPen(QColor(40, 110, 255)));
    ui->temp6Plot->axisRect()->setupFullAxesBox();
    ui->temp6Plot->xAxis->setRange(0, PLOTSIZE);
    ui->temp6Plot->xAxis->setTicker(timeTicker);

    ui->temp6Plot->replot();
}

void GeneralTab::setValue(unsigned int valueType, float newValue)
{
    QString value;
    value.setNum(newValue);

    switch (valueType) {
    case TEMPERATURE_SENS_1_ID:
        setTemperature1(newValue);
        break;
    case TEMPERATURE_SENS_2_ID:
        setTemperature2(newValue);
        break;
    case TEMPERATURE_SENS_3_ID:
        setTemperature3(newValue);
        break;
    case TEMPERATURE_SENS_4_ID:
        setTemperature4(newValue);
        break;
    case TEMPERATURE_SENS_5_ID:
        setTemperature5(newValue);
        break;
    case TEMPERATURE_SENS_6_ID:
        setTemperature6(newValue);
    break;
    // STATUS VALUES
    case EXPERIMENT_STATUS_ID:
        setExperimentStatus(newValue);
        break;
    case STATE_MACHINE_ID:
        setStateMachine(newValue);
        break;
    case CONTAINER_1_STATUS_ID:
        setContainer1Status(newValue);
        break;
    case CONTAINER_2_STATUS_ID:
        setContainer2Status(newValue);
        break;
    case CONTAINER_3_STATUS_ID:
        setContainer3Status(newValue);
        break;
    case COMM_CONNECTION_ID:
        setConnectionStatus(newValue);
        break;
    case COMM_LASTMESSAGERECEIVED:
        setTimeLastMessageReceived(newValue);
        break;
    }
}

void GeneralTab::setTemperature1(float newValue)
{
    qDebug() << "setTemperature1: " << newValue;

    // calculate two new data points:
    static QTime time(QTime::currentTime());
    // calculate two new data points:
    double key = time.elapsed()/1000.0;
    //double key = timeLastMessageReceived./1000.0;
    // add data to lines:
    ui->temp1Plot->graph(0)->addData(key, newValue);
    // rescale value (vertical) axis to fit the current data:
    ui->temp1Plot->graph(0)->rescaleValueAxis(true);
    // make key axis range scroll with the data (at a constant range size of 8):
    ui->temp1Plot->xAxis->setRange(key, PLOTSIZE, Qt::AlignRight);
    ui->temp1Plot->replot();
}

void GeneralTab::setTemperature2(float newValue)
{
    qDebug() << "setTemperature2: " << newValue;

    // calculate two new data points:
    static QTime time(QTime::currentTime());
    // calculate two new data points:
    double key = time.elapsed()/1000.0;
    //double key = timeLastMessageReceived./1000.0;
    // add data to lines:
    ui->temp2Plot->graph(0)->addData(key, newValue);
    // rescale value (vertical) axis to fit the current data:
    ui->temp2Plot->graph(0)->rescaleValueAxis(true);
    // make key axis range scroll with the data (at a constant range size of 8):
    ui->temp2Plot->xAxis->setRange(key, PLOTSIZE, Qt::AlignRight);
    ui->temp2Plot->replot();
}

void GeneralTab::setTemperature3(float newValue)
{
    qDebug() << "setTemperature3: " << newValue;

    // calculate two new data points:
    static QTime time(QTime::currentTime());
    // calculate two new data points:
    double key = time.elapsed()/1000.0;
    //double key = timeLastMessageReceived./1000.0;
    // add data to lines:
    ui->temp3Plot->graph(0)->addData(key, newValue);
    // rescale value (vertical) axis to fit the current data:
    ui->temp3Plot->graph(0)->rescaleValueAxis(true);
    // make key axis range scroll with the data (at a constant range size of 8):
    ui->temp3Plot->xAxis->setRange(key, PLOTSIZE, Qt::AlignRight);
    ui->temp3Plot->replot();
}

void GeneralTab::setTemperature4(float newValue)
{
    qDebug() << "setTemperature4: " << newValue;

    // calculate two new data points:
    static QTime time(QTime::currentTime());
    // calculate two new data points:
    double key = time.elapsed()/1000.0;
    //double key = timeLastMessageReceived./1000.0;
    // add data to lines:
    ui->temp4Plot->graph(0)->addData(key, newValue);
    // rescale value (vertical) axis to fit the current data:
    ui->temp4Plot->graph(0)->rescaleValueAxis(true);
    // make key axis range scroll with the data (at a constant range size of 8):
    ui->temp4Plot->xAxis->setRange(key, PLOTSIZE, Qt::AlignRight);
    ui->temp4Plot->replot();
}

void GeneralTab::setTemperature5(float newValue)
{
    qDebug() << "setTemperature5: " << newValue;

    // calculate two new data points:
    static QTime time(QTime::currentTime());
    // calculate two new data points:
    double key = time.elapsed()/1000.0;
    //double key = timeLastMessageReceived./1000.0;
    // add data to lines:
    ui->temp5Plot->graph(0)->addData(key, newValue);
    // rescale value (vertical) axis to fit the current data:
    ui->temp5Plot->graph(0)->rescaleValueAxis(true);
    // make key axis range scroll with the data (at a constant range size of 8):
    ui->temp5Plot->xAxis->setRange(key, PLOTSIZE, Qt::AlignRight);
    ui->temp5Plot->replot();
}

void GeneralTab::setTemperature6(float newValue)
{
    qDebug() << "setTemperature6: " << newValue;

    // calculate two new data points:
    static QTime time(QTime::currentTime());
    // calculate two new data points:
    double key = time.elapsed()/1000.0;
    //double key = timeLastMessageReceived./1000.0;
    // add data to lines:
    ui->temp6Plot->graph(0)->addData(key, newValue);
    // rescale value (vertical) axis to fit the current data:
    ui->temp6Plot->graph(0)->rescaleValueAxis(true);
    // make key axis range scroll with the data (at a constant range size of 8):
    ui->temp6Plot->xAxis->setRange(key, PLOTSIZE, Qt::AlignRight);
    ui->temp6Plot->replot();
}

void GeneralTab::setExperimentStatus(float newValue)
{
    qDebug() << "setExperimentStatus: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Experiment_Status::EXP_OK:
        newValueStr = "OK";
        break;
    case Experiment_Status::EXP_ERROR:
        newValueStr = "ERROR";
        break;
    case Experiment_Status::EXP_WARNING:
        newValueStr = "WARNING";
        break;
    }
    ui->experimentStatusLabelValue->setText(newValueStr);
}

void GeneralTab::setStateMachine(float newValue)
{
    qDebug() << "setStateMachine: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case State_Machine_Status::PASSIVE:
        newValueStr = "PASSIVE";
        break;
    case State_Machine_Status::MANUAL:
        newValueStr = "MANUAL";
        break;
    case State_Machine_Status::AUTONOMOUS:
        newValueStr = "AUTONOMOUS";
        break;
    }
    ui->stateLabelValue->setText(newValueStr);
}

void GeneralTab::setContainer1Status(float newValue)
{
    qDebug() << "setContainer1Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Container_Status::CONT_OK:
        newValueStr = "OK";
        break;
    case Container_Status::CONT_ERROR:
        newValueStr = "ERROR";
        break;
    case Container_Status::CONT_RELEASED:
        newValueStr = "RELEASED";
        break;
    }
    ui->cont1StatusLabelValue->setText(newValueStr);
}

void GeneralTab::setContainer2Status(float newValue)
{
    qDebug() << "setContainer2Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Container_Status::CONT_OK:
        newValueStr = "OK";
        break;
    case Container_Status::CONT_ERROR:
        newValueStr = "ERROR";
        break;
    case Container_Status::CONT_RELEASED:
        newValueStr = "RELEASED";
        break;
    }
    ui->cont2StatusLabelValue->setText(newValueStr);
}

void GeneralTab::setContainer3Status(float newValue)
{
    qDebug() << "setContainer3Status: " << newValue;

    QString newValueStr("UNKNOWN");

    switch ((int) newValue) {
    case Container_Status::CONT_OK:
        newValueStr = "OK";
        break;
    case Container_Status::CONT_ERROR:
        newValueStr = "ERROR";
        break;
    case Container_Status::CONT_RELEASED:
        newValueStr = "RELEASED";
        break;
    }
    ui->cont3StatusLabelValue->setText(newValueStr);
}

void GeneralTab::setConnectionStatus(float newValue)
{
    qDebug() << "setConnectionStatus: " << newValue;

    QString newValueStr("UNKNOWN");
    if ((int) newValue == Communication_Status::COM_CONNECTED)
    {
        newValueStr = "CONNECTED";
    }
    else if ((int) newValue == Communication_Status::COM_DISCONNECTED)
    {
        newValueStr = "DISCONNECTED";
    }

    ui->connectLabelValue->setText(newValueStr);

}

void GeneralTab::setTimeLastMessageReceived(float newValue)
{
    qDebug() << "setConnectionStatus: " << newValue;

    QString hourStr = QString::number(((int) newValue) / 10000);
    QString minuteStr = QString::number((((int) newValue) % 10000) / 100);
    QString secondStr = QString::number(((int) newValue) % 100);

    if (hourStr.length() < 2)
    {
        hourStr = "0" + hourStr;
    }

    if (minuteStr.length() < 2)
    {
        minuteStr = "0" + minuteStr;
    }

    if (secondStr.length() < 2)
    {
        secondStr = "0" + secondStr;
    }

    QString newValueStr = hourStr + ":" + minuteStr + ":" + secondStr;
    timeLastMessageReceived.fromString(newValueStr, "%h:%m:%s");
    ui->experimentTimeLabelValue->setText(newValueStr);
}

void GeneralTab::on_setPassiveModeButton_clicked()
{
    emit sendCommand(CMD_PASSIVE_MODE);
}

void GeneralTab::on_setManualModeButton_clicked()
{
    emit sendCommand(CMD_MANUAL_MODE);
}

void GeneralTab::on_setAutonomousModeButton_clicked()
{
    emit sendCommand(CMD_AUTONOMOUS_MODE);
}

void GeneralTab::on_releaseContainer1Button_clicked()
{
    emit sendCommand(CMD_RELEASE_CONTAINER_1);
}

void GeneralTab::on_releaseContainer2Button_clicked()
{
    emit sendCommand(CMD_RELEASE_CONTAINER_2);
}

void GeneralTab::on_releaseContainer3Button_clicked()
{
    emit sendCommand(CMD_RELEASE_CONTAINER_3);
}

void GeneralTab::on_activateMotor1Button_clicked()
{
    emit sendCommand(CMD_ACTIVATE_MOTOR_1);
}

void GeneralTab::on_activateMotor2Button_clicked()
{
    emit sendCommand(CMD_ACTIVATE_MOTOR_2);
}

void GeneralTab::on_deactivateMotor1Button_clicked()
{
    emit sendCommand(CMD_DEACTIVATE_MOTOR_1);
}

void GeneralTab::on_deactivateMotor2Button_clicked()
{
    emit sendCommand(CMD_DEACTIVATE_MOTOR_2);
}

void GeneralTab::on_activateHeater1Button_clicked()
{
    emit sendCommand(CMD_ACTIVATE_HEATER_1);
}

void GeneralTab::on_activateHeater2Button_clicked()
{
    emit sendCommand(CMD_ACTIVATE_HEATER_2);
}

void GeneralTab::on_activateHeater3Button_clicked()
{
    emit sendCommand(CMD_ACTIVATE_HEATER_3);
}

void GeneralTab::on_activateHeater4Button_clicked()
{
    emit sendCommand(CMD_ACTIVATE_HEATER_4);
}

void GeneralTab::on_activateHeater5Button_clicked()
{
    emit sendCommand(CMD_ACTIVATE_HEATER_5);
}

void GeneralTab::on_deactivateHeater1Button_clicked()
{
    emit sendCommand(CMD_DEACTIVATE_HEATER_1);
}

void GeneralTab::on_deactivateHeater2Button_clicked()
{
    emit sendCommand(CMD_DEACTIVATE_HEATER_2);
}

void GeneralTab::on_deactivateHeater3Button_clicked()
{
    emit sendCommand(CMD_DEACTIVATE_HEATER_3);
}

void GeneralTab::on_deactivateHeater4Button_clicked()
{
    emit sendCommand(CMD_DEACTIVATE_HEATER_4);
}

void GeneralTab::on_deactivateHeater5Button_clicked()
{
    emit sendCommand(CMD_DEACTIVATE_HEATER_5);
}

void GeneralTab::on_cameraSystemOnButton_clicked()
{
    emit sendCommand(CMD_CAMERA_MANAGER_ON);
}

void GeneralTab::on_cameraSystemOffButton_clicked()
{
    emit sendCommand(CMD_CAMERA_MANAGER_OFF);
}

void GeneralTab::on_retractServo1Button_clicked()
{
    emit sendCommand(CMD_RETRACT_SERVO_1);
}

void GeneralTab::on_retractServo2Button_clicked()
{
    emit sendCommand(CMD_RETRACT_SERVO_2);
}

void GeneralTab::on_retractServo3Button_clicked()
{
    emit sendCommand(CMD_RETRACT_SERVO_3);
}

void GeneralTab::on_cameraSystemRecordButton_clicked()
{
    emit sendCommand(CMD_CAMERA_MANAGER_REC);
}

void GeneralTab::on_cameraSystemStopButton_clicked()
{
    emit sendCommand(CMD_CAMERA_MANAGER_STOP);
}

void GeneralTab::on_i2cBusResetButton_clicked()
{
    emit sendCommand(CMD_RESET_I2C_BUS);
}

void GeneralTab::on_changeRelGapTimeButton_clicked()
{
    unsigned int timeInSeconds = ui->changeRelGapTimeEdit->text().toUInt(nullptr, 10);

    if (timeInSeconds >= 120)
    {
        emit sendCommandWithVariable(CMD_CHANGE_RELEASE_GAP_TIME, timeInSeconds);
    }
}

void GeneralTab::on_setLaunchTimeButton_clicked()
{
    if (ui->setLaunchTimeCheckBox->isEnabled())
    {
        emit sendCommand(CMD_SET_LAUNCH_TIME);
    }
}

GeneralTab::~GeneralTab()
{
    delete ui;
}
