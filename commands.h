#ifndef COMMANDS_H
#define COMMANDS_H

#define CMD_PASSIVE_MODE               0x00
#define CMD_ACTIVE_MODE                0x01
#define CMD_MANUAL_MODE                0x02
#define CMD_AUTONOMOUS_MODE            0x03

#define CMD_RELEASE_CONTAINER_1        0x04
#define CMD_RELEASE_CONTAINER_2        0x05
#define CMD_RELEASE_CONTAINER_3        0x06
#define CMD_ACTIVATE_MOTOR_1           0x07
#define CMD_DEACTIVATE_MOTOR_1         0x08
#define CMD_ACTIVATE_MOTOR_2           0x09
#define CMD_DEACTIVATE_MOTOR_2         0x0A
#define CMD_ACTIVATE_HEATER_1          0x0B
#define CMD_DEACTIVATE_HEATER_1        0x0C
#define CMD_ACTIVATE_HEATER_2          0x0D
#define CMD_DEACTIVATE_HEATER_2        0x0E
#define CMD_ACTIVATE_HEATER_3          0x0F
#define CMD_DEACTIVATE_HEATER_3        0x10
#define CMD_ACTIVATE_HEATER_4          0x11
#define CMD_DEACTIVATE_HEATER_4        0x12
#define CMD_ACTIVATE_HEATER_5          0x13
#define CMD_DEACTIVATE_HEATER_5        0x14
#define CMD_CHANGE_RELEASE_GAP_TIME    0x15
#define CMD_CAMERA_MANAGER_ON          0x16
#define CMD_CAMERA_MANAGER_OFF         0x17
#define CMD_RETRACT_SERVO_1            0x18
#define CMD_RETRACT_SERVO_2            0x19
#define CMD_RETRACT_SERVO_3            0x1A
#define CMD_CAMERA_MANAGER_REC         0x1B
#define CMD_CAMERA_MANAGER_STOP        0x1C
#define CMD_RESET_I2C_BUS              0x1D
#define CMD_SET_LAUNCH_TIME            0x1E

#endif // COMMANDS_H
