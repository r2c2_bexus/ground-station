#include "r2c2databasedriver.h"


#include <QSqlError>
#include <QDebug>

R2C2DatabaseDriver::R2C2DatabaseDriver(QObject *parent) : QObject(parent)
{
       r2c2DB = QSqlDatabase::addDatabase("QMYSQL");
       r2c2DB.setHostName("localhost");
       r2c2DB.setDatabaseName("r2c2database");
       r2c2DB.setUserName("R2C2");
       r2c2DB.setPassword("R2C2kiruna;");
       bool ok = r2c2DB.open();

       qDebug() << "R2C2 DB Open: " << ok;

       if(!ok)
       {
           QSqlError dbError = r2c2DB.lastError();
           qDebug() << "DB Error: " << dbError.text();
       }
}

/*CREATE TABLE status_data (
id int NOT NULL,
time_of_message datetime NOT NULL,
num_message int NOT NULL,
temperature1 float NOT NULL,
temperature2 float NOT NULL,
temperature3 float NOT NULL,
temperature4 float NOT NULL,
temperature5 float NOT NULL,
temperature6 float NOT NULL,
pressure float NOT NULL,
humidity float NOT NULL,
latitude float NOT NULL,
longitude float NOT NULL,
altitude float NOT NULL,
PRIMARY KEY (id)
);*/

/* CREATE TABLE sensor_data (
 * id int NOT NULL,
 * time_of_message datetime NOT NULL,
 * num_message int NOT NULL,
 * temperature1 float NOT NULL,
 * temperature2 float NOT NULL,
 * temperature3 float NOT NULL,
 * temperature4 float NOT NULL,
 * temperature5 float NOT NULL,
 * temperature6 float NOT NULL,
 * pressure float NOT NULL,
 * humidity float NOT NULL,
 * latitude float NOT NULL,
 * longitude float NOT NULL,
 * altitude float NOT NULL,
 * PRIMARY KEY (id)
 * );
*/

R2C2DatabaseDriver::~R2C2DatabaseDriver()
{
    if (r2c2DB.isOpen())
    {
        r2c2DB.close();
    }
}
