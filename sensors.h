#ifndef SENSORS_H
#define SENSORS_H

#define TEMPERATURE_SENS_1_ID   1
#define TEMPERATURE_SENS_2_ID   2
#define TEMPERATURE_SENS_3_ID   3
#define TEMPERATURE_SENS_4_ID   4
#define TEMPERATURE_SENS_5_ID   5
#define TEMPERATURE_SENS_6_ID   6
#define HUMIDITY_SENS_ID        7
#define GPS_LATITUDE_ID         8
#define GPS_LONGITUDE_ID        9
#define GPS_ALTITUDE_ID        10
#define PRESSURE_SENS_ID       11

#endif // SENSORS_H
