#ifndef CONNECTOR_H
#define CONNECTOR_H

#include <QObject>
#include "r2c2tab.h"
#include "r2c2communicationnode.h"
#include "r2c2filedriver.h"

class Connector : public QObject
{
    Q_OBJECT
public:
    explicit Connector(QObject *parent = nullptr): QObject(parent) {}

    void addDataSubscriber(R2C2Tab &tab, R2C2CommunicationNode &commNode);
    void addCommandPublisher(R2C2Tab &tab, R2C2CommunicationNode &commNode);
    void addMessageSubscriber(R2C2FileDriver &fileDriver, R2C2CommunicationNode &commNode);
};

#endif // CONNECTOR_H
